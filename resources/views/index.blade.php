<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>急速贷款</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('css/app.css')}}?v=123123123" rel="stylesheet">
    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?65f6e6f6acd4164480cd94eca7907f56";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</head>
<body>
<div class="container">
    <ul class="nav">
        <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
               aria-expanded="false">
                推荐产品 <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li role="presentation" @if($sortType == 0)class="active" @endif><a href="{{ route('website.index',['sortType'=>0]) }}">默认排序</a></li>
                <li role="presentation" @if($sortType == 1)class="active" @endif><a href="{{ route('website.index',['sortType'=>1]) }}">利率由低到高</a></li>
                <li role="presentation" @if($sortType == 2)class="active" @endif><a href="{{ route('website.index',['sortType'=>2]) }}">额度由高到底</a></li>
                <li role="presentation" @if($sortType == 3)class="active" @endif ><a href="{{ route('website.index',['sortType'=>3]) }}">期限由高到低</a></li>
            </ul>
        </li>
    </ul>
    <div class="widget-loan" data-example-id="default-media">
        @foreach($products as $product)
        <a href="{{ route('product.show',['id'=>$product->id]) }}" class="loan-link" onclick="_hmt.push(['_trackEvent', 'product', 'detail', '{{$product->name}}']);" >
            <div class="media">
                <div class="media-left">
                    <img class="avatar64" alt="{{ $product->name }}" src="{{ route('website.image.show',['image_name'=>$product->logo]) }}">
                </div>
                <div class="media-body">
                    <div class="row-item">
                        <span class="loan_name">{{ $product->name }}</span>
                        <span class="pull-right">{{ $product->slogan }}</span>
                    </div>
                    <div class="row-item">
                        <span class="loan_money">{{ format_loan_money($product->loan_money_min) }}-{{ format_loan_money($product->loan_money_max) }}</span>
                        <span class="pull-right">@if($product->loan_term_unit==2){{$product->loan_term_max}}天 @else {{$product->loan_term_min}}-{{ $product->loan_term_max }}月 @endif/{{ format_loan_interest($product->loan_interest) }}月息</span>
                    </div>
                    @if($product->tags)
                    <div class="row-item">
                        @foreach($product->tags as $tag)
                        <span class="{{ $tag['class'] }}">{{ $tag['name'] }}</span>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </a>
        @endforeach
    </div>
</div>
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>

</body>
</html>