<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>急速贷款</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="col-md-6 col-md-offset-3 bg-white login-wrap mt-30">
        <h1 class="h4 text-center text-muted login-title">用户登录</h1>
        <form role="form" name="loginForm" action="{{ route('website.login') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group @if($errors->has('mobile')) has-error @endif">
                <input type="text" class="form-control" id="mobile" name="mobile" value="{{ old('mobile') }}" required="" placeholder="请填写11位手机号码">
                @if($errors->has('mobile'))<div class="help-block">{{ $errors->first('mobile') }}</div> @endif
            </div>
            <div class="form-group @if($errors->has('code')) has-error @endif">
                <div class="row">
                    <div class="col-xs-8">
                        <input name="code" id="code" type="text" maxlength="6" placeholder="收到的手机验证码" class="form-control" value="">
                        @if($errors->has('code'))<div class="help-block">{{ $errors->first('code') }}</div> @endif
                    </div>
                    <div class="col-xs-4"><button class="btn btn-xl btn-default btn-send-code" data-mobile_id="mobile"   type="button">发送验证码</button></div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">提 交</button>
            </div>
        </form>
    </div>
</div>
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
<script src="{{ asset('js/global.js') }}"></script>
<script type="text/javascript">

    var countdown_second = 60;
    function Countdown(btn) {
        if (countdown_second == 0) {
            btn.attr("class","btn btn-xl btn-default btn-send-code");
            btn.text("免费获取验证码");
            countdown_second = 60;
            return;
        } else {
            btn.attr("class","btn btn-xl btn-default disabled");
            btn.text("重新发送(" + countdown_second + ")");
            countdown_second--;
        }
        setTimeout(function() {
                Countdown(btn) }
            ,1000)
    }


    $(function(){
        $(".btn-send-code").click(function(){
            var mobile = $("#mobile").val();
            $.post('/user/sendSmsCode',{mobile:mobile},function(msg){
                if( msg.code == 0 ){
                    Countdown($(".btn-send-code"));
                }else{
                    show_form_error($("#mobile"),msg.message);
                    return false;
                }
            });
        });
    });

</script>
<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?65f6e6f6acd4164480cd94eca7907f56";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
</body>
</html>