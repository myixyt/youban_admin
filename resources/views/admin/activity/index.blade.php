@extends('admin/public/layout')

@section('title')
    活动管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            活动列表
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.activity.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.activity.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="row">
                                    <form name="searchForm" action="" method="GET">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-4 hidden-xs">
                                            <input type="text" class="form-control" name="username" placeholder="昵称" value="{{ $filter['username'] or '' }}"/>
                                        </div>
                                        <div class="col-xs-2">
                                            <select class="form-control" name="status">
                                                <option value="0">状态</option>
                                                @foreach(trans_article_status('all') as $key=>$val)
                                                <option value="{{ $key }}" @if(isset($filter['status']) && $filter['status'] == $key)selected @endif>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" name="date_range" id="date_range" class="form-control" placeholder="时间范围" value="{{ $filter['date_range'] or '' }}" />
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><input type="checkbox" class="checkbox-toggle"/></th>
                                        <th>话题</th>
                                        <th>举办日期</th>
                                        <th>地址</th>
                                        <th>排序</th>
                                        <th>状态</th>
                                        <th>设为本期</th>
                                        <th>修改时间</th>
                                        <th>操作</th>
                                    </tr>
                                    @foreach($activities as $activity)
                                    <tr @if($activity['is_current']) class="active" @endif>
                                        <td><input type="checkbox" value="{{ $activity['id'] }}" name="id[]"/></td>
                                        <td>{{ $activity['topic'] }}</td>
                                        <td>{{ $activity['preferred_date'] }}</td>
                                        <td>{{ $activity['address'] }}</td>
                                        <td>{{ $activity['sort'] }}</td>
                                        <td><span class="label @if($activity['status'] == 1) label-success @else label-danger @endif">{{ trans_article_status($activity['status']) }}</span></td>
                                        <td><span data-id = {{ $activity['id'] }} class="label @if($activity['is_current'] == 1) label-success @else label-default setCurrent @endif"> @if(!$activity['is_current']) 设为本期 @else 本期 @endif</span></td>
                                        <td>{{ $activity['updated_at'] }}</td>
                                        <td>
                                            <div class="btn-group-xs" >
                                                <a class="btn btn-default" href="{{ route('admin.activity.edit',['id'=>$activity['id']]) }}" data-toggle="tooltip" title="编辑信息"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.activity.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.activity.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 {{ $activities->total() }} 条数据</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_list',"{{ route('admin.activity.index') }}");
        $(function () {
            $(".setCurrent").on('click',function () {
                $.post('{{ route('admin.activity.setCurrent') }}',{
                    '_token':'{{ csrf_token() }}',
                    'id':$(this).attr("data-id")
                },function (data) {
                    window.location.reload();
                })
            })
        })

    </script>
@endsection