@extends('admin/public/layout')
@section('title')编辑活动@endsection
@section('content')
    <section class="content-header">
        <h1>
            编辑活动
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" enctype="multipart/form-data" method="POST" action="{{ route('admin.activity.update',['id'=>$activity['id']]) }}">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group @if ($errors->has('topic')) has-error @endif">
                                <label for="topic">主题</label>
                                <input type="text" class="form-control" name="topic" value="{{ old('topic',$activity['topic']) }}"/>
                                @if($errors->has('topic')) <p class="help-block">{{ $errors->first('topic') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('address')) has-error @endif">
                                <label for="address">活动地址</label>
                                <input type="text" class="form-control" name="address" value="{{ old('address',$activity['address']) }}"/>
                                @if($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
                            </div>

                            <div class="form-group">
                                <label for="setting-birthday">举办日期</label>
                                <span class="text-muted">(例：2019-01-01)</span>
                                <input name="preferred_date" value="{{ old('preferred_date',$activity['preferred_date']) }}" class="form-control">
                                @if($errors->has('preferred_date')) <p class="help-block">{{ $errors->first('preferred_date') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('preferred_time')) has-error @endif">
                                <label for="preferred_time">举办时间</label>
                                <span class="text-muted"></span>
                                <input type="text" class="form-control" name="preferred_time" value="{{ old('preferred_time',$activity['preferred_time']) }}"/>
                                @if($errors->has('preferred_time')) <p class="help-block">{{ $errors->first('preferred_time') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('url')) has-error @endif">
                                <label for="url">活动详情链接</label>
                                <input type="text" class="form-control" name="url" value="{{ old('url',$activity['url']) }}"/>
                                @if($errors->has('url')) <p class="help-block">{{ $errors->first('url') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('apply_url')) has-error @endif">
                                <label for="apply_url">申请链接</label>
                                <input type="text" class="form-control" name="apply_url" value="{{ old('apply_url',$activity['apply_url']) }}"/>
                                @if($errors->has('apply_url')) <p class="help-block">{{ $errors->first('apply_url') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('live_url')) has-error @endif">
                                <label for="live_url">直播链接</label>
                                <input type="text" class="form-control" name="live_url" value="{{ old('live_url',$activity['live_url']) }}"/>
                                @if($errors->has('live_url')) <p class="help-block">{{ $errors->first('live_url') }}</p> @endif
                            </div>

                            <div class="form-group">
                                <label>参加专家</label>
                                <div class="checkbox">
                                    @foreach(load_experts() as $key=>$val)
                                        <label>
                                            <input name="experts[]" type="checkbox" value="{{ $key }}" @if(in_array($key,$activity['experts'])) checked @endif> {{ $val }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('logo')) has-error @endif">
                                <label>logo</label>
                                @if($activity['logo'])
                                    <div style="margin-top: 10px;">
                                        <img src="{{route('website.image.show',['image_name'=>$activity['logo']])}}" style="width:100px;height:100px;">
                                    </div>
                                @endif
                                <input type="file" class="form-control" name="logo"/>
                            </div>

                            <div class="form-group @if ($errors->has('sort')) has-error @endif">
                                <label for="sort">排序</label>
                                <span class="text-muted">(请填写数字，数字越小越靠前显示)</span>
                                <input type="text" class="form-control" name="sort" placeholder="0" value="{{ old('sort',$activity['sort']) }}"/>
                                @if($errors->has('sort')) <p class="help-block">{{ $errors->first('sort') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('status')) has-error @endif">
                                <label for="status">状态</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_article_status('all') as $key=>$val)
                                        <label><input type="radio" name="status" value="{{ $key }}" @if($key == $activity['status'])checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary editor-submit">保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            $("#birthday").datepicker({
                format: "yyyy-mm-dd",
                language: "zh-CN",
                calendarWeeks: true,
                autoclose: true
            });
            set_active_menu('manage_list',"{{ route('admin.activity.index') }}");
        });
    </script>
@endsection