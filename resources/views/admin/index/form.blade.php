@extends('admin/public/layout')
@section('title')表单示例@endsection
@section('css')
    <link href="{{ asset('/static/js/summernote/summernote.css')}}" rel="stylesheet">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            表单示例
            <small>表单示例</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" method="POST" action="">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="website_url">单选</label>
                                <span class="text-muted">(单选)</span>
                                <div class="radio">
                                    <label><input type="radio" name="register_open" value="1" checked > 允许 </label>
                                    <label class="ml-20"><input type="radio" name="register_open" value="0" > 拒绝 </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>下拉菜单</label>
                                <select class="form-control" name="type">
                                        <option value=""></option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>下拉菜单</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Check me out
                                    </label>
                                    <label>
                                        <input type="checkbox"> Check me out
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="website_url">输入框</label>
                                <span class="text-muted">(设置为0代表不做任何限制)</span>
                                <input type="text" class="form-control" name="register_limit_num" placeholder="0为不限制" value="{{ old('register_limit_num',0) }}"  />
                            </div>

                            <div class="form-group @if ($errors->has('logo')) has-error @endif">
                                <label>图片上传</label>
                                <input type="file" class="form-control" name="file"  />
                            </div>

                            <div class="form-group">
                                <label for="register_editor">文本输入框</label>
                                <div id="register_editor">{!! old('register_license','') !!}</div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <input type="hidden" id="register_editor_content"  name="register_license" />
                            <button type="submit" class="btn btn-primary editor-submit" >保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

    <script src="{{ asset('/static/js/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('/static/js/summernote/lang/summernote-zh-CN.min.js') }}"></script>
    <script type="text/javascript">
        $(function(){
            set_active_menu('manage_user',"{{ route('admin.index.form') }}");

            $('#register_editor').summernote({
                lang: 'zh-CN',
                height: 300,
                placeholder:'完善用户注册协议',
                toolbar: [ {!! config('zhanqun.summernote.article') !!} ],
                callbacks: {
                    onChange:function (contents, $editable) {
                        var code = $(this).summernote("code");
                        $("#register_editor_content").val(code);
                    },
                    onImageUpload: function(files) {
                        upload_editor_image(files[0],'register_editor');
                    }
                }
            });

        });
    </script>
@endsection