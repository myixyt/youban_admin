@extends('admin/public/layout')

@section('title')
    用户管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            用户列表
            <small>显示当前系统的所有注册用户</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="btn-group">
                                    <a href="" class="btn btn-default btn-sm" data-toggle="tooltip" title="创建新用户"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="通过审核" onclick="confirm_submit('item_form','','确认审核通过选中项？')"><i class="fa fa-check-square-o"></i></button>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项" onclick="confirm_submit('item_form','','确认删除选中项？')"><i class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="row">
                                    <form name="searchForm" action="" method="GET">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-2 hidden-xs">
                                            <input type="text" class="form-control" name="user_id" placeholder="UID" value=""/>
                                        </div>
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" class="form-control" name="word" placeholder="用户名|邮箱" value=""/>
                                        </div>
                                        <div class="col-xs-2">
                                            <select class="form-control" name="status">
                                                <option value="-9">不选择</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" name="date_range" id="date_range" class="form-control" placeholder="时间范围" value="" />
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><input type="checkbox" class="checkbox-toggle"/></th>
                                        <th>用户ID</th>
                                        <th>用户名</th>
                                        <th>手机</th>
                                        <th>邮箱</th>
                                        <th>身份职业</th>
                                        <th>地区</th>
                                        <th>注册ip</th>
                                        <th>更新时间</th>
                                        <th>状态</th>
                                        <th>操作</th>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" value="" name="id[]"/></td>
                                        <td>1</td>
                                        <td>张三</td>
                                        <td>13888888888</td>
                                        <td>admin@haodai.net</td>
                                        <td>运营</td>
                                        <td>北京</td>
                                        <td>127.0.0.1</td>
                                        <td>2018-02-09</td>
                                        <td><span class="label label-success">已审核</span> </td>
                                        <td>
                                            <div class="btn-group-xs" >
                                                <a class="btn btn-default" href="" data-toggle="tooltip" title="编辑用户信息"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" value="" name="id[]"/></td>
                                        <td>1</td>
                                        <td>李四</td>
                                        <td>18888888838</td>
                                        <td>admin@haodai.net</td>
                                        <td>运营</td>
                                        <td>北京</td>
                                        <td>127.0.0.1</td>
                                        <td>2018-02-19</td>
                                        <td><span class="label label-danger">待审核</span> </td>
                                        <td>
                                            <div class="btn-group-xs" >
                                                <a class="btn btn-default" href="" data-toggle="tooltip" title="编辑用户信息"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="btn-group">
                                    <a href="" class="btn btn-default btn-sm" data-toggle="tooltip" title="创建新用户"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="通过审核" onclick="confirm_submit('item_form','','确认审核通过选中项？')"><i class="fa fa-check-square-o"></i></button>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项" onclick="confirm_submit('item_form','','确认删除选中项？')"><i class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 2 条数据</span>
                                    {{--{!! str_replace('/?', '?', $users->appends($filter)->links()) !!}--}}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_user',"{{ route('admin.index.list') }}");
    </script>
@endsection