<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>管理后台登录</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset('/static/css/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/static/js/scojs/sco.message.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{ asset('/static/css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="{{ asset('/css/admin/admin.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('js/respond.min.js') }}"></script>
    <![endif]-->


</head>
<body class="login-page">
<div class="login-box">
    <div class="login-box-body">
        <p class="login-box-msg">管理后台</p>
        <form action="{{route('admin.index.login')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group has-feedback @if ($errors->first('username')) has-error @endif">
                <input type="text" name="username" class="form-control" placeholder="账号" value=""/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback @if ($errors->first('password')) has-error @endif">
                <input type="password" name="password" class="form-control" placeholder="密码"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->first('password'))
                 <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="captcha"  class="form-control" type="captcha" placeholder="验证码" style="width:150px;display:inline-block" name="captcha" value="{{ old('captcha')  }}" required>
                <img src="{{captcha_src()}}" style="cursor: pointer;" onclick="this.src='{{captcha_src()}}'+Math.random()">
                @if($errors->has('captcha'))
                    <div class="col-md-12">
                        <p class="text-danger text-left"><strong>{{$errors->first('captcha')}}</strong></p>
                    </div>
                @endif
            </div>


            <div class="form-group has-feedback">
                <button class="btn bg-olive btn-block btn-flat">登录</button>
            </div>
        </form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.3 -->
<script type="text/javascript" src="{{ asset('/static/js/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script type="text/javascript" src="{{ asset('/static/css/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/global.js') }}"></script>

</body>
</html>