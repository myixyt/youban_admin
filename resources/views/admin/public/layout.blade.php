<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') - 柚伴管理后台</title>


    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset('/static/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/static/scojs/sco.message.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/static/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/static/icheck/all.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{ asset('/static/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/static/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/static/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/static/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/static/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
    <!-- Theme style -->
    <link href="{{ asset('/css/admin/admin.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('/css/admin/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <link type="text/css" rel="stylesheet" href="{{ asset('/static/select/css/bootstrap-select.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/static/webuploader/webuploader.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/static/summernote/dist/summernote.css') }}">

    @yield('css')

</head>
<body class="skin-blue sidebar-mini @if($sidebar_collapse) sidebar-collapse @endif">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->

        <div class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>T</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg text-center">
                柚伴
                <!--<a class="navbar-brand admin_logo" href="{{ route('admin.index.index') }}"></a>-->
            </span>

        </div>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" id="sliderbar_control"  data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs">{{ Auth()->user('admin')->username }}</span>
                        </a>
                        <ul class="dropdown-menu"  style="width:100px;left:-20px;">
                            <!-- User image -->
                            <!--
                            <li class="user-header">
                                <<img src="" class="img-circle" alt="User Image" />
                                <p>
                                    <small></small>
                                </p>
                            </li>-->
                            <!-- Menu Footer-->
                            <li class="user-footer" style="width:100px;">
                                {{--<div class="pull-left">--}}
                                    {{--<a href="#" class="btn btn-default btn-flat">修改密码</a>--}}
                                {{--</div>--}}
                                <div class="pull-right">
                                    <a href="{{ url('admin/logout') }}" class="btn btn-default btn-flat">退出登录</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
           @include('admin/public/menu')
        </section>
        <!-- /.sidebar -->



    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @yield('content')
    </div><!-- /.content-wrapper -->

    @include('admin/public/footer')

</div><!-- ./wrapper -->

<!--scripts-->
@include('admin.public.script')

@yield('script')


</body>
</html>
