<ul class="sidebar-menu" id="root_menu">
    <li class="header">管理菜单</li>
    <li><a href="{{ route('admin.index.index') }}"><i class="fa fa-dashboard"></i> <span>首页</span> </a></li>

    <li class="treeview">
        <a href="#">
            <i class="fa fa-user"></i> <span>用户管理</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu" id="manage_users">
            @if(auth()->user()->id === 1)
            <li><a href="{{ route('admin.admin.index') }}"><i class="fa fa-circle-o"></i> 管理员列表</a></li>
            @endif
            <li><a href="{{ route('admin.user.index') }}"><i class="fa fa-circle-o"></i> 用户列表</a></li>
        </ul>
    </li>
    @if(auth()->user()->id === 1)
    <li class="treeview">
        <a href="#">
            <i class="fa fa-commenting"></i> <span>内容管理</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu" id="manage_contents">
            <li><a href="{{ route('admin.banner.index') }}"><i class="fa fa-circle-o"></i> Banner管理</a></li>
            <li><a href="{{ route('admin.article.index') }}"><i class="fa fa-circle-o"></i> 文章管理</a></li>
            <li><a href="{{ route('admin.question.index') }}"><i class="fa fa-circle-o"></i> 病例列表</a></li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#">
            <i class="fa fa-commenting"></i> <span>积分管理</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu" id="manage_applications">
            <li><a href="{{ route('admin.application.index') }}"><i class="fa fa-circle-o"></i> 积分列表</a></li>
        </ul>
    </li>
    @endif
</ul>
