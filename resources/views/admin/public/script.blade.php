
<script type="text/javascript">
    //公告js变量设置
{{--    var site_url = "{{ route('website.index') }}" ;--}}
</script>


<!-- jQuery 2.1.3 -->
<script type="text/javascript" src="{{ asset('/static/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/static/jquery.cookie.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script type="text/javascript" src="{{ asset('/static/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- AdminLTE App -->
<script src='{{ asset('/js/admin/admin.js') }}' type="text/javascript"></script>
<!-- AdminLTE for demo purposes -->
<script src='{{ asset('/js/admin/common.js') }}' type="text/javascript"></script>


<!-- Slimscroll -->
<script src="{{ asset('/static/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<!-- FastClick -->
<script src='{{ asset('/static/fastclick.min.js') }}' type="text/javascript"></script>
<!-- icheck -->
<script src="{{ asset('/static/icheck/icheck.min.js') }}" type="text/javascript"></script>
<!--sco.js-->
<script src="{{ asset('/static/scojs/sco.message.js') }}" type="text/javascript"></script>
<!--daterangepicker.js-->
<script src="{{ asset('/static/daterangepicker/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/static/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('/static/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/static/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js') }}"></script>
<script src="{{ asset('/static/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

<!-- AdminLTE App -->

<!-- AdminLTE for demo purposes -->

<script src="{{ asset('/static/select2/js/select2.min.js')}}"></script>



<!-- Latest compiled and minified JavaScript -->
<script src="{{ asset('/static/select/js/bootstrap-select.min.js')}}"></script>
<script src="{{ asset('/static/select/js/i18n/defaults-zh_CN.min.js')}}"></script>
<script src="{{ asset('/static/webuploader/webuploader.js')}}"></script>

<script src="{{ asset('/static/summernote/dist/summernote.js')}}"></script>


@if ( session('message') )
<script type="text/javascript">
    $(function(){$.scojs_message('{{ session('message') }}',{{ session('message_type') }});});
</script>
@endif