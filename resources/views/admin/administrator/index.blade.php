@extends('admin/public/layout')

@section('title')
    管理员管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            管理员列表
            <small>显示当前系统的所有管理员</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.admin.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加"><i class="fa fa-plus"></i></a>
                                    {{--<button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.admin.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>--}}
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="row">
                                    <form name="searchForm" action="" method="GET">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-2 hidden-xs">
                                            <input type="text" class="form-control" name=mobile" placeholder="手机号" value="{{ $filter['mobile'] or '' }}"/>
                                        </div>
                                        {{--<div class="col-xs-2">
                                            <select class="form-control" name="status">
                                                <option value="-9">状态</option>
                                                @foreach(trans_product_status('all') as $key=>$val)
                                                <option value="{{ $key }}" @if(isset($filter['status']) && $filter['status'] == $key)selected @endif>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>--}}
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" name="date_range" id="date_range" class="form-control" placeholder="时间范围" value="{{ $filter['date_range'] or '' }}" />
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><input type="checkbox" class="checkbox-toggle"/></th>
                                        <th>手机号</th>
                                        <th>管理员账号</th>
                                        <th>状态</th>
                                        <th>创建时间</th>
                                        <th>操作</th>
                                    </tr>
                                    @foreach($admins as $admin)
                                    <tr>
                                        <td><input type="checkbox" value="{{ $admin['id'] }}" name="id[]"/></td>
                                        <td>{{ $admin['mobile'] }}</td>
                                        <td>{{ $admin['username'] }}</td>
                                        <td>{{ trans_common_status($admin['status']) }}</td>
                                        <td>{{ $admin['created_at'] }}</td>
                                        <td>
                                            <div class="btn-group-xs" >
                                                <a class="btn btn-default" href="{{ route('admin.admin.edit',['id'=>$admin['id']]) }}" data-toggle="tooltip" title="编辑信息"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.admin.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加"><i class="fa fa-plus"></i></a>
                                    {{--<button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.admin.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>--}}
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 {{ $admins->total() }} 条数据</span>
                                    {!! str_replace('/?', '?', $admins->appends($filter)->links()) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_users',"{{ route('admin.admin.index') }}");
    </script>
@endsection