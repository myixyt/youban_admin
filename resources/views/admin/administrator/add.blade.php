@extends('admin/public/layout')
@section('title')添加管理员@endsection
@section('css')
    <link href="{{ asset('/static/js/summernote/summernote.css')}}" rel="stylesheet">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            添加管理员
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" enctype="multipart/form-data" method="POST" action="{{ route('admin.admin.store') }}">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group @if ($errors->has('username')) has-error @endif">
                                <label for="username">用户名</label>
                                <span>最少6个字符</span>
                                <input type="text" class="form-control" name="username" required value="{{ old('username','') }}"/>
                                @if($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('password')) has-error @endif">
                                <label for="password">密码</label>
                                <input type="text" class="form-control" name="password" required value="{{ old('password','') }}"/>
                                @if($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('mobile')) has-error @endif">
                                <label for="mobile">手机号</label>
                                <input type="text" class="form-control" name="mobile" required value="{{ old('mobile','') }}"/>
                                @if($errors->has('mobile')) <p class="help-block">{{ $errors->first('mobile') }}</p> @endif
                            </div>

                         {{--   <div class="form-group @if ($errors->has('mobile')) has-error @endif">
                                <label>渠道</label>
                                <select class="form-control" name="channel_id">
                                    <option value="0">请选择渠道</option>
                                @foreach(load_channel_list() as $key=>$value)
                                        <option value="{{ $key }}">{{$value}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('channel_id')) <p class="help-block">{{ $errors->first('channel_id') }}</p> @endif
                            </div>--}}

                            <div class="form-group @if ($errors->has('status')) has-error @endif">
                                <label for="status">状态</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_common_status('all') as $key=>$val)
                                        <label><input type="radio" name="status" value="{{ $key }}" @if($key ==1)checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary editor-submit">保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            set_active_menu('manage_users', "{{ route('admin.admin.index') }}");
        });
    </script>
@endsection