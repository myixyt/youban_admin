@extends('admin/public/layout')
@section('title')编辑医生@endsection
@section('content')
    <section class="content-header">
        <h1>
            编辑医生
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" enctype="multipart/form-data" method="POST" action="{{ route('admin.expert.update',['id'=>$expert['id']]) }}">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group @if ($errors->has('username')) has-error @endif">
                                <label for="title">昵称</label>
                                <input type="text" class="form-control" name="username" value="{{ old('username',$expert['username']) }}"/>
                                @if($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('title')) has-error @endif">
                                <label for="title">职称</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title',$expert['title']) }}"/>
                                @if($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('logo')) has-error @endif">
                                <label>logo</label>
                                @if($expert['logo'])
                                    <div style="margin-top: 10px;">
                                        <img src="{{route('website.image.show',['image_name'=>$expert['logo']])}}" style="width:100px;height:100px;">
                                    </div>
                                @endif
                                <input type="file" class="form-control" name="logo"/>
                            </div>

                            <div class="form-group @if ($errors->has('summary')) has-error @endif">
                                <label for="register_editor">简介</label>
                                <textarea class="form-control" style="height: 100px;" name="summary">{{ old('summary',$expert['summary']) }}</textarea>
                                @if($errors->has('summary')) <p class="help-block">{{ $errors->first('summary') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('sort')) has-error @endif">
                                <label for="sort">排序</label>
                                <span class="text-muted">(请填写数字，数字越小越靠前显示)</span>
                                <input type="text" class="form-control" name="sort" placeholder="0" value="{{ old('sort',$expert['sort']) }}"/>
                                @if($errors->has('sort')) <p class="help-block">{{ $errors->first('sort') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('status')) has-error @endif">
                                <label for="status">状态</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_article_status('all') as $key=>$val)
                                        <label><input type="radio" name="status" value="{{ $key }}" @if($key == $expert['status'])checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary editor-submit">保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            set_active_menu('manage_activities',"{{ route('admin.expert.index') }}");
        });
    </script>
@endsection