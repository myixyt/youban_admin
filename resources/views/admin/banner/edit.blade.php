@extends('admin/public/layout')
@section('title')活动管理@endsection

@section('content')
    <section class="content-header">
        <h1>
            添加关于活动
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" enctype="multipart/form-data" method="POST" action="{{ route('admin.banner.update',['id'=>$banner['id']]) }}">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="type" value="{{ $banner['type'] }}">
                        <div class="box-body">
                            <div class="form-group @if ($errors->has('url')) has-error @endif">
                                <label for="url">链接</label>
                                <input type="text" class="form-control" name="url" value="{{ old('url',$banner['url']) }}"/>
                                @if($errors->has('url')) <p class="help-block">{{ $errors->first('url') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('summary')) has-error @endif">
                                <label for="summary">简介</label>
                                <input type="text" class="form-control" name="summary" value="{{ old('summary',$banner['summary']) }}"/>
                                @if($errors->has('summary')) <p class="help-block">{{ $errors->first('summary') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('logo')) has-error @endif">
                                <label>logo</label>
                                @if($banner['logo'])
                                    <div style="margin-top: 10px;">
                                        <img src="{{route('website.image.show',['image_name'=>$banner['logo']])}}" style="width:100px;height:100px;">
                                    </div>
                                @endif
                                <input type="file" class="form-control" name="logo"/>
                            </div>

                            <div class="form-group @if ($errors->has('sort')) has-error @endif">
                                <label for="sort">排序</label>
                                <span class="text-muted">(请填写数字，数字越小越靠前显示)</span>
                                <input type="text" class="form-control" name="sort" placeholder="0" value="{{ old('sort',$banner['sort']) }}"/>
                                @if($errors->has('sort')) <p class="help-block">{{ $errors->first('sort') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('status')) has-error @endif">
                                <label for="status">状态</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_article_status('all') as $key=>$val)
                                        <label><input type="radio" name="status" value="{{ $key }}" @if($key == $banner['status'])checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary editor-submit">保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            set_active_menu('manage_contents',"{{ route('admin.banner.index') }}");

        });
    </script>
@endsection