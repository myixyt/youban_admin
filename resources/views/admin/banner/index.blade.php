@extends('admin/public/layout')

@section('title')
    Banner管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Banner管理列表
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.banner.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.banner.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-9">
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><input type="checkbox" class="checkbox-toggle"/></th>
                                        <th>链接</th>
                                        <th>简介</th>
                                        <th>排序</th>
                                        <th>状态</th>
                                        <th>创建时间</th>
                                        <th>操作</th>
                                    </tr>
                                    @foreach($banners as $banner)
                                    <tr>
                                        <td><input type="checkbox" value="{{ $banner['id'] }}" name="id[]"/></td>
                                        <td>{{ $banner['url'] }}</td>
                                        <td>{{ str_limit($banner['summary'],150) }}</td>
                                        <td>{{ $banner['sort'] }}</td>
                                        <td><span class="label @if($banner['status'] == 1) label-success @else label-danger @endif">{{ trans_article_status($banner['status']) }}</span></td>
                                        <td>{{ $banner['created_at'] }}</td>
                                        <td>
                                            <div class="btn-group-xs" >
                                                <a class="btn btn-default" href="{{ route('admin.banner.edit',['id'=>$banner['id']]) }}" data-toggle="tooltip" title="编辑信息"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.banner.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.banner.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 {{ $banners->total() }} 条数据</span>
                                    {!! str_replace('/?', '?', $banners->appends($filter)->links()) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_contents',"{{ route('admin.banner.index') }}");
    </script>
@endsection