@extends('admin/public/layout')

@section('title')
    产品管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            产品列表
            <small>显示当前系统的所有产品</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.product.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加新产品"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.product.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="row">
                                    <form name="searchForm" action="" method="GET">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-2 hidden-xs">
                                            <input type="text" class="form-control" name="name" placeholder="产品名称" value="{{ $filter['name'] or '' }}"/>
                                        </div>
                                        <div class="col-xs-2">
                                            <select class="form-control" name="status">
                                                <option value="-9">状态</option>
                                                @foreach(trans_product_status('all') as $key=>$val)
                                                <option value="{{ $key }}" @if(isset($filter['status']) && $filter['status'] == $key)selected @endif>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" name="date_range" id="date_range" class="form-control" placeholder="时间范围" value="{{ $filter['date_range'] or '' }}" />
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><input type="checkbox" class="checkbox-toggle"/></th>
                                        <th>产品名称</th>
                                        <th>标语</th>
                                        <th>贷款额度(元)</th>
                                        <th>还款期限</th>
                                        <th>月利息(分)</th>
                                        <th>排序</th>
                                        <th>状态</th>
                                        <th>操作</th>
                                    </tr>
                                    @foreach($products as $product)
                                    <tr>
                                        <td><input type="checkbox" value="{{ $product['id'] }}" name="id[]"/></td>
                                        <td>{{ $product['name'] }}</td>
                                        <td>{{ str_limit($product['slogan'],50) }}</td>
                                        <td>{{ $product['loan_money_min'].'-'.$product['loan_money_max'] }}</td>
                                        <td>{{ $product['loan_term_min'].'-'.$product['loan_term_max'].trans_loan_term_unit($product['loan_term_unit']) }}</td>
                                        <td>{{ $product['loan_interest'] }}</td>
                                        <td>{{ $product['sort'] }}</td>
                                        <td>{{ trans_product_status($product['status']) }}</td>
                                        <td>
                                            <div class="btn-group-xs" >
                                                <a class="btn btn-default" href="{{ route('admin.product.edit',['id'=>$product['id']]) }}" data-toggle="tooltip" title="编辑产品信息"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.product.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加新产品"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.product.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 {{ $products->total() }} 条数据</span>
                                    {!! str_replace('/?', '?', $products->appends($filter)->links()) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_products',"{{ route('admin.product.index') }}");
    </script>
@endsection