@extends('admin/public/layout')
@section('title')编辑产品@endsection
@section('css')
    <link href="{{ asset('/static/js/summernote/summernote.css')}}" rel="stylesheet">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            编辑产品
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" enctype="multipart/form-data" method="POST" action="{{ route('admin.product.update',['id'=>$product['id']]) }}">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group @if ($errors->has('name')) has-error @endif">
                                <label for="name">产品名称</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name',$product['name']) }}"/>
                                @if($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('slogan')) has-error @endif">
                                <label for="slogan">产品标语</label>
                                <input type="text" class="form-control" name="slogan" value="{{ old('slogan',$product['slogan']) }}"/>
                                @if($errors->has('slogan')) <p class="help-block">{{ $errors->first('slogan') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('url')) has-error @endif">
                                <label for="url">产品链接</label>
                                <input type="text" class="form-control" name="url" value="{{ old('url',$product['url']) }}"/>
                                @if($errors->has('url')) <p class="help-block">{{ $errors->first('url') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('loan_money_min')) has-error @endif">
                                <label for="loan_term_max">还款最小额度</label>
                                <span class="text-muted">(单位：元)</span>
                                <input type="text" class="form-control" name="loan_money_min" value="{{ old('loan_money_min',$product['loan_money_min']) }}"/>
                                @if($errors->has('loan_money_min')) <p class="help-block">{{ $errors->first('loan_money_min') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('loan_money_max')) has-error @endif">
                                <label for="loan_money_max">贷款最大额度</label>
                                <span class="text-muted">(单位：元)</span>
                                <input type="text" class="form-control" name="loan_money_max" value="{{ old('loan_money_max',$product['loan_money_max']) }}"/>
                                @if($errors->has('loan_money_max')) <p class="help-block">{{ $errors->first('loan_money_max') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('loan_term_unit')) has-error @endif">
                                <label for="loan_term_unit">贷款期限类型</label>
                                <span class="text-muted">(贷款时间单位)</span>
                                <div class="radio">
                                    @foreach(trans_loan_term_unit('all') as $key=>$val)
                                        <label><input type="radio" name="loan_term_unit" value="{{ $key }}" @if($key == $product['loan_term_unit'])checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('loan_term_min')) has-error @endif">
                                <label for="loan_term_min">还款最小期限</label>
                                <span class="text-muted">(单位与贷款期限类型一致)</span>
                                <input type="text" class="form-control" name="loan_term_min" value="{{ old('loan_term_min',$product['loan_term_min']) }}"/>
                                @if($errors->has('loan_term_min')) <p class="help-block">{{ $errors->first('loan_term_min') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('loan_term_max')) has-error @endif">
                                <label for="loan_term_max">还款最大期限</label>
                                <span class="text-muted">(单位与贷款期限类型一致)</span>
                                <input type="text" class="form-control" name="loan_term_max" value="{{ old('loan_term_max',$product['loan_term_max']) }}"/>
                                @if($errors->has('loan_term_max')) <p class="help-block">{{ $errors->first('loan_term_max') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('loan_interest')) has-error @endif">
                                <label for="loan_term_max">月利息</label>
                                <span class="text-muted">(单位：分)</span>
                                <input type="text" class="form-control" name="loan_interest" value="{{ old('loan_interest',$product['loan_interest']) }}"/>
                                @if($errors->has('loan_interest')) <p class="help-block">{{ $errors->first('loan_interest') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('tags')) has-error @endif">
                                <label>产品标签</label>
                                <div class="checkbox">
                                    @foreach(config('zhanqun.tags') as $key=>$value)
                                    <label>
                                        <input name="tags[]" type="checkbox" value="{{ $value['id'] }}" @if(in_array($value['id'],explode(',',$product['tags'])))checked @endif> {{ $value['name'] }}
                                    </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('logo')) has-error @endif">
                                <label>产品logo</label>
                                @if($product['logo'])
                                    <div style="margin-top: 10px;">
                                        <img src="{{route('website.image.show',['image_name'=>$product['logo']])}}" style="width:100px;height:100px;">
                                    </div>
                                @endif
                                <input type="file" class="form-control" name="logo"/>
                            </div>

                            <div class="form-group @if ($errors->has('sort')) has-error @endif">
                                <label for="sort">排序</label>
                                <span class="text-muted">(数字越小越靠前展示)</span>
                                <input type="text" class="form-control" name="sort" value="{{ old('sort',$product['sort']) }}"/>
                                @if($errors->has('sort')) <p class="help-block">{{ $errors->first('sort') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('status')) has-error @endif">
                                <label for="status">产品状态</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_product_status('all') as $key=>$val)
                                        <label><input type="radio" name="status" value="{{ $key }}" @if($key ==$product['status'])checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary editor-submit">保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            set_active_menu('manage_products', "{{ route('admin.product.index') }}");
        });
    </script>
@endsection