@extends('admin/public/layout')
@section('title')添加文章@endsection
@section('css')
    <link href="{{ asset('/static/js/summernote/summernote.css')}}" rel="stylesheet">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            添加文章
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" enctype="multipart/form-data" method="POST" action="{{ route('admin.article.store') }}">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group @if ($errors->has('title')) has-error @endif">
                                <label for="title">文章名称</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title','') }}"/>
                                @if($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('type')) has-error @endif">
                                <label for="type">文章类型</label>
                                <span class="text-muted">(请选择文章模块)</span>
                                <div class="radio">
                                    @foreach(trans_article_type('all') as $key=>$val)
                                        <label><input type="radio" name="type" value="{{ $key }}" @if($key ==9)checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('source')) has-error @endif">
                                <label for="source">文章来源</label>
                                <input type="text" class="form-control" name="source" value="{{ old('source','') }}"/>
                                @if($errors->has('source')) <p class="help-block">{{ $errors->first('source') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('keywords')) has-error @endif">
                                <label for="keywords">文章关键词</label>
                                <span class="text-muted">(关键词)</span>
                                <input type="text" class="form-control" name="keywords" value="{{ old('keywords','') }}"/>
                                @if($errors->has('keywords')) <p class="help-block">{{ $errors->first('keywords') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('logo')) has-error @endif">
                                <label>文章logo</label>
                                <input type="file" class="form-control" name="logo"/>
                            </div>

                            <div class="form-group">
                                <label for="setting-birthday">发布日期</label>
                                <span class="text-muted">(例：2019-01-01)</span>
                                <input name="publish_date" id="birthday" type="text" placeholder="格式 YYYY-MM-DD" value="{{ old('publish_date','') }}" class="form-control">
                                @if($errors->has('publish_date')) <p class="help-block">{{ $errors->first('publish_date') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('status')) has-error @endif">
                                <label for="status">文章状态</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_article_status('all') as $key=>$val)
                                        <label><input type="radio" name="status" value="{{ $key }}" @if($key ==1)checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('content')) has-error @endif">
                            <label for="register_editor">文章内容</label>
                                <div id="register_editor">{!! old('content','') !!}</div>
                                @if($errors->has('content')) <p class="help-block">{{ $errors->first('content') }}</p> @endif
                            </div>

                        </div>
                        <div class="box-footer">
                            <input type="hidden" id="register_editor_content"  name="content" />
                            <button type="submit" class="btn btn-primary editor-submit">保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/static/js/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('/static/js/summernote/lang/summernote-zh-CN.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            set_active_menu('manage_contents', "{{ route('admin.article.index') }}");
            /*生日日历*/
            $("#birthday").datepicker({
                format: "yyyy-mm-dd",
                language: "zh-CN",
                calendarWeeks: true,
                autoclose: true
            });
            $('#register_editor').summernote({
                lang: 'zh-CN',
                height: 300,
                placeholder:'请输入文章内容',
                toolbar: [ {!! config('zhanqun.summernote.article') !!} ],
                callbacks: {
                    onChange:function (contents, $editable) {
                        var code = $(this).summernote("code");
                        $("#register_editor_content").val(code);
                    },
                    onImageUpload: function(files) {
                        upload_editor_image(files[0],'register_editor');
                    }
                }
            });
        });
    </script>
@endsection