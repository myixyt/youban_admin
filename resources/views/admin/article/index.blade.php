@extends('admin/public/layout')

@section('title')
    文章管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            文章列表
            <small>显示当前系统的所有文章</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.article.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加文章"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.article.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="row">
                                    <form name="searchForm" action="" method="GET">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-4 hidden-xs">
                                            <input type="text" class="form-control" name="title" placeholder="文章名称" value="{{ $filter['title'] or '' }}"/>
                                        </div>
                                        <div class="col-xs-2">
                                            <select class="form-control" name="type">
                                                <option value="-9">文章类型</option>
                                                @foreach(trans_article_type('all') as $key=>$val)
                                                <option value="{{ $key }}" @if(isset($filter['type']) && $filter['type'] == $key)selected @endif>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" name="date_range" id="date_range" class="form-control" placeholder="时间范围" value="{{ $filter['date_range'] or '' }}" />
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><input type="checkbox" class="checkbox-toggle"/></th>
                                        <th>文章名称</th>
                                        <th>类型</th>
                                        <th>来源</th>
                                        <th>关键词</th>
                                        <th>状态</th>
                                        <th>更新时间</th>
                                        <th>操作</th>
                                    </tr>
                                    @foreach($articles as $article)
                                    <tr>
                                        <td><input type="checkbox" value="{{ $article['id'] }}" name="id[]"/></td>
                                        <td>{{ str_limit($article['title'],50) }}</td>
                                        <td>{{ trans_article_type($article['type']) }}</td>
                                        <td>{{ $article['source'] }}</td>
                                        <td>{{ $article['keywords'] }}</td>
                                        <td><span class="label @if($article['status'] == 1) label-success @else label-danger @endif">{{ trans_article_status($article['status']) }}</span></td>
                                        <td>{{ $article['created_at'] }}</td>
                                        <td>
                                            <div class="btn-group-xs" >
                                                <a class="btn btn-default" href="{{ route('admin.article.edit',['id'=>$article['id']]) }}" data-toggle="tooltip" title="编辑文章信息"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.article.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加文章"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.article.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 {{ $articles->total() }} 条数据</span>
                                    {!! str_replace('/?', '?', $articles->appends($filter)->links()) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_contents',"{{ route('admin.article.index') }}");
    </script>
@endsection