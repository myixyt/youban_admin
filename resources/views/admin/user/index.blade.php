@extends('admin/public/layout')

@section('title')
    用户管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            用户列表
            <small>显示当前系统的所有用户</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.user.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加用户"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.user.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="row">
                                    <form name="searchForm" action="" method="GET">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-4 hidden-xs">
                                            <input type="text" class="form-control" name="mobile" placeholder="手机号" value="{{ $filter['mobile'] or '' }}"/>
                                        </div>
                                        <div class="col-xs-2">
                                            <select class="form-control" name="role">
                                                <option value="-9">用户类型</option>
                                                @foreach(trans_user_role('all') as $key=>$val)
                                                <option  value="{{ $key }}" @if(isset($filter['role']) && $filter['role'] == $key)selected @endif>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" name="date_range" id="date_range" class="form-control" placeholder="时间范围" value="{{ $filter['date_range'] or '' }}" />
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><input type="checkbox" class="checkbox-toggle"/></th>
                                        <th>昵称</th>
                                        <th>手机号</th>
                                        <th>绑定手机</th>
                                        <th>角色</th>
                                        <th>微信号</th>
                                        <th>扫码人数</th>
                                        <th>注册时间</th>
                                        <th>备注</th>
                                        <th>操作</th>
                                    </tr>
                                    @foreach($users as $user)
                                    <tr>
                                        <td><input type="checkbox" value="{{ $user['id'] }}" name="id[]"/></td>
                                        <td>{{ $user->nickname }}</td>
                                        <td>{{ $user->mobile }}</td>
                                        <td>{{ $user->hasOneDoctor->mobile }}</td>
                                        <td>{{ trans_user_role($user->role) }}</td>
                                        <td>{{ $user->wechat }}</td>
                                        <td>{{ $user->views }}</td>
                                        <td>{{ $user->created_at }}</td>
                                        <td>{{ str_limit($user->note,30) }}</td>
                                        <td>
                                            <div class="btn-group-xs" >
                                                <a class="btn btn-default" href="{{ route('admin.user.edit',['id'=>$user['id']]) }}" data-toggle="tooltip" title="编辑用户信息"><i class="fa fa-edit"></i></a>
                                                @if($user->role ==1)
                                                <a class="btn btn-default" href="{{ route('website.ajax.sendSmsCode',['code'=>$user['code'],'id'=>$user['id']]) }}" data-toggle="tooltip" title="查看二维码"><i class="fa fa-eye"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.user.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加用户"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.user.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 {{ $users->total() }} 条数据</span>
                                    {!! str_replace('/?', '?', $users->appends($filter)->links()) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_users',"{{ route('admin.user.index') }}");
    </script>
@endsection