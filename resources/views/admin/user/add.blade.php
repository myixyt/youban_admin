@extends('admin/public/layout')
@section('title')添加用户@endsection
@section('css')
    <link href="{{ asset('/static/js/summernote/summernote.css')}}" rel="stylesheet">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            添加用户
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" enctype="multipart/form-data" method="POST" action="{{ route('admin.user.store') }}">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group @if ($errors->has('nickname')) has-error @endif">
                                <label for="nickname">姓名</label>
                                <input type="text" class="form-control" name="nickname" value="{{ old('nickname','') }}"/>
                                @if($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('sex')) has-error @endif">
                                <label for="status">医生性别</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_sex_type('all') as $key=>$val)
                                        <label><input type="radio" name="sex" value="{{ $key }}" @if($key ==1)checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('title')) has-error @endif">
                                <label for="title">职称</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title','') }}"/>
                                @if($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('age')) has-error @endif">
                                <label for="source">年龄</label>
                                <small>必须为整数</small>
                                <input type="text" class="form-control" name="age" value="{{ old('age','') }}"/>
                                @if($errors->has('age')) <p class="help-block">{{ $errors->first('age') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('mobile')) has-error @endif">
                                <label for="source">手机号码</label>
                                <input type="text" class="form-control" name="mobile" value="{{ old('mobile','') }}"/>
                                @if($errors->has('mobile')) <p class="help-block">{{ $errors->first('mobile') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('wechat')) has-error @endif">
                                <label for="source">微信</label>
                                <input type="text" class="form-control" name="wechat" value="{{ old('wechat','') }}"/>
                                @if($errors->has('wechat')) <p class="help-block">{{ $errors->first('wechat') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('youzan_url')) has-error @endif">
                                <label for="source">有赞分销链接</label>
                                <input type="text" class="form-control" name="youzan_url" value="{{ old('youzan_url','') }}"/>
                                @if($errors->has('youzan_url')) <p class="help-block">{{ $errors->first('youzan_url') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('logo')) has-error @endif">
                                <label>医生头像</label>
                                <input type="file" class="form-control" name="logo"/>
                            </div>

                            <div class="form-group @if ($errors->has('content')) has-error @endif">
                            <label for="register_editor">医生简介</label>
                                <div id="register_editor">{!! old('content','') !!}</div>
                                @if($errors->has('content')) <p class="help-block">{{ $errors->first('content') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('status')) has-error @endif">
                                <label for="status">医生状态</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_article_status('all') as $key=>$val)
                                        <label><input type="radio" name="status" value="{{ $key }}" @if($key ==1)checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <input type="hidden" id="register_editor_content"  name="content" />
                            <button type="submit" class="btn btn-primary editor-submit">保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/static/js/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('/static/js/summernote/lang/summernote-zh-CN.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            set_active_menu('manage_users',"{{ route('admin.user.index') }}");
            /*生日日历*/
            $("#birthday").datepicker({
                format: "yyyy-mm-dd",
                language: "zh-CN",
                calendarWeeks: true,
                autoclose: true
            });
            $('#register_editor').summernote({
                lang: 'zh-CN',
                height: 300,
                placeholder:'请输入医生简介',
                toolbar: [ {!! config('zhanqun.summernote.article') !!} ],
                callbacks: {
                    onChange:function (contents, $editable) {
                        var code = $(this).summernote("code");
                        $("#register_editor_content").val(code);
                    },
                    onImageUpload: function(files) {
                        upload_editor_image(files[0],'register_editor');
                    }
                }
            });
        });
    </script>
@endsection