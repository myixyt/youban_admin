@extends('admin/public/layout')
@section('title')添加肺腑之言@endsection
@section('css')
    <link href="{{ asset('/static/js/summernote/summernote.css')}}" rel="stylesheet">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            添加肺腑之言
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" enctype="multipart/form-data" method="POST" action="{{ route('admin.letter.store') }}">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <div class="box-body">
                            <div class="form-group @if ($errors->has('username')) has-error @endif">
                                <label for="username">昵称</label>
                                <input type="text" class="form-control" name="username" value="{{ old('username','') }}"/>
                                @if($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('sort')) has-error @endif">
                                <label for="sort">排序</label>
                                <span class="text-muted">(请填写数字，数字越小越靠前显示)</span>
                                <input type="text" class="form-control" name="sort" placeholder="0" value="{{ old('sort',0) }}"/>
                                @if($errors->has('sort')) <p class="help-block">{{ $errors->first('sort') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('status')) has-error @endif">
                                <label for="status">状态</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_article_status('all') as $key=>$val)
                                        <label><input type="radio" name="status" value="{{ $key }}" @if($key ==1)checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('title')) has-error @endif">
                                <label for="title">标题</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title','') }}"/>
                                @if($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('content')) has-error @endif">
                            <label for="register_editor">内容</label>
                                <div id="register_editor">{!! old('content','') !!}</div>
                                @if($errors->has('content')) <p class="help-block">{{ $errors->first('content') }}</p> @endif
                            </div>

                        </div>
                        <div class="box-footer">
                            <input type="hidden" id="register_editor_content"  name="content" value="{!! old('content','')!!} "/>
                            <button type="submit" class="btn btn-primary editor-submit">保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/static/js/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('/static/js/summernote/lang/summernote-zh-CN.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            set_active_menu('manage_contents', "{{ route('admin.letter.index') }}");
            $('#register_editor').summernote({
                lang: 'zh-CN',
                height: 300,
                placeholder:'请输入内容',
                toolbar: [ {!! config('zhanqun.summernote.article') !!} ],
                callbacks: {
                    onChange:function (contents, $editable) {
                        var code = $(this).summernote("code");
                        $("#register_editor_content").val(code);
                    },
                    onImageUpload: function(files) {
                        upload_editor_image(files[0],'register_editor');
                    }
                }
            });
        });
    </script>
@endsection