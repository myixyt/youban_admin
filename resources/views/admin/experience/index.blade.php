@extends('admin/public/layout')

@section('title')
    病友实录管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            病友列表
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.experience.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.experience.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="row">
                                    <form name="searchForm" action="" method="GET">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-4 hidden-xs">
                                            <input type="text" class="form-control" name="username" placeholder="昵称" value="{{ $filter['username'] or '' }}"/>
                                        </div>
                                        <div class="col-xs-2">
                                            <select class="form-control" name="status">
                                                <option value="0">状态</option>
                                                @foreach(trans_article_status('all') as $key=>$val)
                                                <option value="{{ $key }}" @if(isset($filter['status']) && $filter['status'] == $key)selected @endif>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" name="date_range" id="date_range" class="form-control" placeholder="时间范围" value="{{ $filter['date_range'] or '' }}" />
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><input type="checkbox" class="checkbox-toggle"/></th>
                                        <th>标题</th>
                                        <th>昵称</th>
                                        <th>年龄</th>
                                        <th>疾病</th>
                                        <th>地区</th>
                                        <th>排序</th>
                                        <th>状态</th>
                                        <th>创建时间</th>
                                        <th>操作</th>
                                    </tr>
                                    @foreach($experiences as $experience)
                                    <tr>
                                        <td><input type="checkbox" value="{{ $experience['id'] }}" name="id[]"/></td>
                                        <td>{{ str_limit($experience['title'],30) }}</td>
                                        <td>{{ $experience['username'] }}</td>
                                        <td>{{ $experience['age'] }}</td>
                                        <td>{{ $experience['disease'] }}</td>
                                        <td>{{ $experience['district'] }}</td>
                                        <td>{{ $experience['sort'] }}</td>
                                        <td><span class="label @if($experience['status'] == 1) label-success @else label-danger @endif">{{ trans_article_status($experience['status']) }}</span></td>
                                        <td>{{ $experience['created_at'] }}</td>
                                        <td>
                                            <div class="btn-group-xs" >
                                                <a class="btn btn-default" href="{{ route('admin.experience.edit',['id'=>$experience['id']]) }}" data-toggle="tooltip" title="编辑信息"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="btn-group">
                                    <a href="{{ route('admin.experience.add') }}" class="btn btn-default btn-sm" data-toggle="tooltip" title="添加"><i class="fa fa-plus"></i></a>
                                    <button class="btn btn-default btn-sm" data-toggle="tooltip" title="删除选中项"
                                            onclick="confirm_submit('item_form','{{ route('admin.experience.destroy') }}','确认删除选中项？')"><i
                                                class="fa fa-trash-o"></i></button>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 {{ $experiences->total() }} 条数据</span>
                                    {!! str_replace('/?', '?', $experiences->appends($filter)->links()) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_contents',"{{ route('admin.experience.index') }}");
    </script>
@endsection