@extends('admin/public/layout')
@section('title')编辑病友实录@endsection
@section('css')
    <link href="{{ asset('/static/js/summernote/summernote.css')}}" rel="stylesheet">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            编辑病友实录
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <form role="form" name="addForm" id="register_form" enctype="multipart/form-data" method="POST" action="{{ route('admin.experience.update',['id'=>$experience['id']]) }}">
                        <input type="hidden" name="_token" id="editor_token" value="{{ csrf_token() }}">
                        <div class="box-body">

                            <div class="form-group @if ($errors->has('title')) has-error @endif">
                                <label for="title">标题</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title',$experience['title']) }}"/>
                                @if($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('username')) has-error @endif">
                                <label for="title">昵称</label>
                                <input type="text" class="form-control" name="username" value="{{ old('username',$experience['username']) }}"/>
                                @if($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('age')) has-error @endif">
                                <label for="age">年龄</label>
                                <input type="text" class="form-control" name="age" value="{{ old('age',$experience['age']) }}"/>
                                @if($errors->has('age')) <p class="help-block">{{ $errors->first('age') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('gender')) has-error @endif">
                                <label for="gender">性别</label>
                                <input type="text" class="form-control" name="gender" value="{{ old('gender',$experience['gender']) }}"/>
                                @if($errors->has('gender')) <p class="help-block">{{ $errors->first('gender') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('district')) has-error @endif">
                                <label for="district">所在地区</label>
                                <input type="text" class="form-control" name="district" value="{{ old('district',$experience['district']) }}"/>
                                @if($errors->has('district')) <p class="help-block">{{ $errors->first('district') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('tel')) has-error @endif">
                                <label for="district">联系方式</label>
                                <input type="text" class="form-control" name="tel" value="{{ old('tel',$experience['tel']) }}"/>
                                @if($errors->has('tel')) <p class="help-block">{{ $errors->first('tel') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('disease')) has-error @endif">
                                <label for="disease">疾病名称</label>
                                <input type="text" class="form-control" name="disease" value="{{ old('disease',$experience['disease']) }}"/>
                                @if($errors->has('disease')) <p class="help-block">{{ $errors->first('disease') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('keywords')) has-error @endif">
                                <label for="keywords">关键词</label>
                                <span class="text-muted">(多个用逗号分隔)</span>
                                <input type="text" class="form-control" name="keywords" value="{{ old('keywords',$experience['keywords']) }}"/>
                                @if($errors->has('keywords')) <p class="help-block">{{ $errors->first('keywords') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('sort')) has-error @endif">
                                <label for="sort">排序</label>
                                <span class="text-muted">(请填写数字，数字越小越靠前显示)</span>
                                <input type="text" class="form-control" name="sort" placeholder="0" value="{{ old('sort',$experience['sort']) }}"/>
                                @if($errors->has('sort')) <p class="help-block">{{ $errors->first('sort') }}</p> @endif
                            </div>

                            <div class="form-group @if ($errors->has('logo')) has-error @endif">
                                <label>logo</label>
                                @if($experience['logo'])
                                    <div style="margin-top: 10px;">
                                        <img src="{{route('website.image.show',['image_name'=>$experience['logo']])}}" style="width:100px;height:100px;">
                                    </div>
                                @endif
                                <input type="file" class="form-control" name="logo"/>
                            </div>

                            <div class="form-group @if ($errors->has('status')) has-error @endif">
                                <label for="status">状态</label>
                                <span class="text-muted"></span>
                                <div class="radio">
                                    @foreach(trans_article_status('all') as $key=>$val)
                                        <label><input type="radio" name="status" value="{{ $key }}" @if($key == $experience['status'])checked @endif> {{ $val }} </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('content')) has-error @endif">
                                <label for="register_editor">内容</label>
                                <div id="register_editor">{!! old('content',$experience['content']) !!}</div>
                                @if($errors->has('content')) <p class="help-block">{{ $errors->first('content') }}</p> @endif
                            </div>

                        </div>
                        <div class="box-footer">
                            <input type="hidden" id="register_editor_content"  name="content" value="{{ $experience['content'] }}" />
                            <button type="submit" class="btn btn-primary editor-submit">保存</button>
                            <button type="reset" class="btn btn-success">重置</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('/static/js/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('/static/js/summernote/lang/summernote-zh-CN.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            set_active_menu('manage_contents', "{{ route('admin.experience.index') }}");
            $('#register_editor').summernote({
                lang: 'zh-CN',
                height: 300,
                placeholder:'请输入内容',
                toolbar: [ {!! config('zhanqun.summernote.article') !!} ],
                callbacks: {
                    onChange:function (contents, $editable) {
                        var code = $(this).summernote("code");
                        $("#register_editor_content").val(code);
                    },
                    onImageUpload: function(files) {
                        upload_editor_image(files[0],'register_editor');
                    }
                }
            });
        });
    </script>
@endsection