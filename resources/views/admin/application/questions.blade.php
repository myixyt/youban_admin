@extends('admin/public/layout')

@section('title')
    病例管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            病例列表
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">

                            </div>
                            <div class="col-xs-9">
                                <div class="row">
                                    <form name="searchForm" action="" method="GET">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-4 hidden-xs">
                                            <input type="text" class="form-control" name="mobile" placeholder="患者手机号" value="{{ $filter['mobile'] or '' }}"/>
                                        </div>
                                        <div class="col-xs-2">
                                            <select class="form-control" name="status">
                                                <option value="0">状态</option>
                                                @foreach(trans_reply_status('all') as $key=>$val)
                                                <option value="{{ $key }}" @if(isset($filter['status']) && $filter['status'] == $key)selected @endif>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" name="date_range" id="date_range" class="form-control" placeholder="时间范围" value="{{ $filter['date_range'] or '' }}" />
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th>ID</th>
                                        <th>患者手机号</th>
                                        <th>医生</th>
                                        <th>医生手机号</th>
                                        <th>提交时间</th>
                                        <th>回复状态</th>
{{--                                        <th>操作</th>--}}
                                    </tr>
                                    @foreach($questions as $question)
                                    <tr>
                                        <td>{{ $question->id }}</td>
                                        <td>{{ $question->user->mobile }}</td>
                                        <td>{{ $question->hasOneUser->nickname }}</td>
                                        <td>{{ $question->hasOneUser->mobile }}</td>
                                        <td>{{ $question['created_at'] }}</td>
                                        <td><span class="label @if($question['status'] == 1) label-success @else label-danger @endif">{{ trans_reply_status($question['status']) }}</span></td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 {{ $questions->total() }} 条数据</span>
                                    {!! str_replace('/?', '?', $questions->appends($filter)->links()) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_contents',"{{ route('admin.question.index') }}");
    </script>
@endsection