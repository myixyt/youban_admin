@extends('admin/public/layout')

@section('title')
    积分管理
@endsection

@section('content')
    <section class="content-header">
        <h1>
            积分列表
            <small></small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-3">

                            </div>
                            <div class="col-xs-9">
                                <div class="row">
                                    <form name="searchForm" action="" method="GET">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-xs-4 hidden-xs">
                                            <input type="text" class="form-control" name="mobile" placeholder="手机号" value="{{ $filter['mobile'] or '' }}"/>
                                        </div>
                                        <div class="col-xs-2">
                                            <select class="form-control" name="status">
                                                <option value="0">状态</option>
                                                @foreach(trans_deal_status('all') as $key=>$val)
                                                <option value="{{ $key }}" @if(isset($filter['status']) && $filter['status'] == $key)selected @endif>{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-3 hidden-xs">
                                            <input type="text" name="date_range" id="date_range" class="form-control" placeholder="时间范围" value="{{ $filter['date_range'] or '' }}" />
                                        </div>
                                        <div class="col-xs-1">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body  no-padding">
                        <form name="itemForm" id="item_form" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th>ID</th>
                                        <th>医生</th>
                                        <th>医生手机号</th>
                                        <th>是否处理</th>
                                        <th>提交时间</th>
                                        <th>处理时间</th>
                                        <th>操作</th>
                                    </tr>
                                    @foreach($applications as $application)
                                    <tr>
                                        <td>{{ $application->id }}</td>
                                        <td>{{ $application->user->nickname }}</td>
                                        <td>{{ $application->mobile }}</td>
                                        <td><span class="label @if($application['status'] == 1) label-success @else label-danger @endif">{{ trans_deal_status($application['status']) }}</span></td>
                                        <td>{{ $application['created_at'] }}</td>
                                        <td>{{ $application['deal_time'] }}</td>
                                        <td>
                                            <span class="label @if($application->status ==0)label-success @else label-danger @endif setSuccess" data-id="{{ $application->id }}" data-status="{{ $application->status }}">@if($application->status ==0)标记为已处理 @else 标记为未处理 @endif</span>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <div class="row">
                            <div class="col-sm-3">
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <span class="total-num">共 {{ $applications->total() }} 条数据</span>
                                    {!! str_replace('/?', '?', $applications->appends($filter)->links()) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        set_active_menu('manage_applications',"{{ route('admin.application.index') }}");
        $('.setSuccess').on('click',function () {
            $.post('{{ route('admin.application.setStatus') }}',{'_token':'{{csrf_token()}}','id':$(this).attr("data-id"),'status':$(this).attr("data-status")},function(data) //第二个参数要传token的值 再传参数要用逗号隔开
            {
                if(data != 1)
                {
                    alert("设置失败");
                }else{
                    window.location.reload();
                }
            });
        })
    </script>
@endsection