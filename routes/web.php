<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function (){
    return view('welcome');
});
Route::get('product/{id}',['as'=>'product.show','middleware'=>'auth:web','uses'=>'ProductController@show'])->where(['id'=>'[0-9]+']);
Route::get('image/{image_name}',['as'=>'website.image.show','uses'=>'ImageController@showImage']);
Route::post('image/upload',['as'=>'website.image.upload','uses'=>'ImageController@upload']);

Route::Group(['namespace'=>'Admin','prefix'=>'admin'],function (){
    Route::Group(['middleware'=>'auth:admin'],function() {
        Route::resource('index', 'IndexController', ['as' => 'admin', 'only' => ['index']]);
        Route::get('index/sidebar', ['as' => 'sidebar', 'uses' => 'IndexController@sidebar']);

        Route::post('image/upload', ['as' => 'admin.image.upload', 'uses' => 'ImageController@upload']);
        Route::post('image/ueupload', ['as' => 'admin.image.ueupload', 'uses' => 'ImageController@ueUpload']);
        //显示图片
        Route::get('image/getImage',['uses'=>'ImageController@getImage']);
        //删除图片
        Route::post('image/delImage',['uses'=>'ImageController@delImage']);

        Route::get("form", ['as' => 'admin.index.form', 'uses' => 'IndexController@formSample']);
        Route::get("list", ['as' => 'admin.index.list', 'uses' => 'IndexController@listSample']);

        // 文章管理
        Route::get("article/index", ['as' => 'admin.article.index', 'uses' => 'ArticleController@index']);
        Route::get("article/add", ['as' => 'admin.article.add', 'uses' => 'ArticleController@add']);
        Route::post("article/store", ['as' => 'admin.article.store', 'uses' => 'ArticleController@store']);
        Route::get("article/{id}", ['as' => 'admin.article.edit', 'uses' => 'ArticleController@edit']);
        Route::post("article/update/{id}", ['as' => 'admin.article.update', 'uses' => 'ArticleController@update']);
        Route::post("article/destroy", ['as' => 'admin.article.destroy', 'uses' => 'ArticleController@destroy']);

        //用户管理
        Route::get("user/index", ['as' => 'admin.user.index', 'uses' => 'UserController@index']);
        Route::get("user/add", ['as' => 'admin.user.add', 'uses' => 'UserController@add']);
        Route::post("user/store", ['as' => 'admin.user.store', 'uses' => 'UserController@store']);
        Route::get("user/{id}", ['as' => 'admin.user.edit', 'uses' => 'UserController@edit']);
        Route::post("user/update/{id}", ['as' => 'admin.user.update', 'uses' => 'UserController@update']);
        Route::post("user/destroy", ['as' => 'admin.user.destroy', 'uses' => 'UserController@destroy']);

        // banner管理
        Route::get("banner", ['as' => 'admin.banner.index', 'uses' => 'BannerController@index']);
        Route::get("banner/add", ['as' => 'admin.banner.add', 'uses' => 'BannerController@add']);
        Route::post("banner/store", ['as' => 'admin.banner.store', 'uses' => 'BannerController@store']);
        Route::get("banner/edit/{id}", ['as' => 'admin.banner.edit', 'uses' => 'BannerController@edit']);
        Route::post("banner/update/{id}", ['as' => 'admin.banner.update', 'uses' => 'BannerController@update']);
        Route::post("banner/destroy", ['as' => 'admin.banner.destroy', 'uses' => 'BannerController@destroy']);

        // 积分列表
        Route::get("applications", ['as' => 'admin.application.index', 'uses' => 'ApplicationController@index']);
        Route::post("applications/setStatus", ['as' => 'admin.application.setStatus', 'uses' => 'ApplicationController@setStatus']);

        // 病例列表
        Route::get("questions", ['as' => 'admin.question.index', 'uses' => 'ApplicationController@questions']);

        // 管理员
        Route::get("admin/index", ['as' => 'admin.admin.index', 'uses' => 'AdministratorController@index']);
        Route::get("admin/add", ['as' => 'admin.admin.add', 'uses' => 'AdministratorController@add']);
        Route::post("admin/store", ['as' => 'admin.admin.store', 'uses' => 'AdministratorController@store']);
        Route::get("admin/{id}", ['as' => 'admin.admin.edit', 'uses' => 'AdministratorController@edit']);
        Route::post("admin/update/{id}", ['as' => 'admin.admin.update', 'uses' => 'AdministratorController@update']);
        Route::post("admin/destroy", ['as' => 'admin.admin.destroy', 'uses' => 'AdministratorController@destroy']);


    });

    Route::any("/login",['as'=>'admin.index.login','uses'=>'IndexController@login']);
    Route::any("logout",['as'=>'admin.index.logout','uses'=>'IndexController@logout']);
});

Route::post('ajax/sendSmsCode',['as'=>'website.ajax.sendSmsCode','uses'=>'AjaxController@sendSmsCode']);
Route::get('qrcode/{id}/{code}',['as'=>'website.ajax.sendSmsCode','uses'=>'AjaxController@getQrcode']);
//Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');
