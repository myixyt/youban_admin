<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::Group(['namespace' => 'Api'], function () {
    Route::any('index/{code}', 'IndexController@index');
    Route::get('articles/{type}/{page}', 'IndexController@articles')->where(['type' => '[0-9]+', 'page' => '[0-9]+']);
    Route::get('articles/detail/{id}', 'IndexController@detail')->where(['id' => '[0-9]+']);
    Route::get('experiences/{page}', 'IndexController@experiences')->where(['page' => '[0-9]+']);
    Route::get('letters/{page}', 'IndexController@letters')->where(['page' => '[0-9]+']);

    Route::post('experience/add', 'IndexController@postExperience');
    Route::post('letter/add', 'IndexController@postLetter');

    Route::post('user/login', 'UserController@login');
    Route::post('user/index', 'UserController@index');

    Route::post('doctor/login', 'UserController@smsLogin');
    Route::post('user/smslogin', 'UserController@userSmsLogin');


    Route::group(['middleware' => 'api.loginVerify'], function () {
        Route::post('questions', 'UserController@questionsList'); // 问题列表
        Route::post('ask', 'UserController@ask'); // 发起提问
        Route::post('user/questions', 'UserController@questions'); // 用户问题
        Route::post('user/apply', 'UserController@apply'); // 积分申请
        Route::post('question/detail', 'UserController@questionDetail'); // 问题详情
        Route::post('question/reply', 'UserController@reply'); // 积分申请

        Route::post('doctor/index', 'UserController@index');// 医生首页
    });
});