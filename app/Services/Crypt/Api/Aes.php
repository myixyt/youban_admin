<?php
/**
 * Created by PhpStorm.
 * User: lxl
 * Date: 2017/5/17
 * Time: 22:58
 */

namespace App\Services\Crypt\Api;


use Illuminate\Support\Facades\Config;

class Aes
{

    public static function Instance()
    {
        CryptAES::setKey(Config::get('api.aes_key_app'));
        CryptAES::setIv(Config::get('api.iv_app'));
    }

    /**
     * 对数据进行加密处理
     * @param $data
     * @return bool|string
     */
    public static function enAes($data)
    {
        if (empty($data)) {
            return false;
        }
        self::Instance();

        return CryptAES::encrypt($data);
    }

    /**
     * 对数据进行解密处理
     * @param $code
     * @return bool|string
     */
    public static function deAes($code)
    {
        if (empty($code)) {
            return false;
        }
        self::Instance();
        return CryptAES::decrypt($code);
    }

    public static function newEnAes($data)
    {
        if (empty($data)) {
            return false;
        }
        self::Instance();

        return CryptAES::newEncrypt($data);
    }

    public static function newDeAes($code)
    {
        if (empty($code)) {
            return false;
        }
        self::Instance();
        return CryptAES::newDecrypt($code);
    }
}