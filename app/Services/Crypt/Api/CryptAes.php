<?php
/**
 * Created by PhpStorm.
 * User: lxl
 * Date: 2017/5/17
 * Time: 22:59
 */

namespace App\Services\Crypt\Api;

class CryptAES
{
    private static $hex_iv = '00000000000000000000000000000000'; # converted JAVA byte code in to HEX and placed it here
    private static $key = 'a_pXutkc616fyfxftvvuZa;ps'; #Same as in JAVA

    public static function setKey($key) {
        self::$key = hash('sha256', $key, true);
    }

    public static function setIv($hexIv) {
        if (!empty($hexIv)) {
            self::$hex_iv =$hexIv;
        }
    }

    public static function encrypt($str) {
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        mcrypt_generic_init($td, self::$key, self::hexToStr(self::$hex_iv));
        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = $block - (strlen($str) % $block);
        $str .= str_repeat(chr($pad), $pad);
        $encrypted = mcrypt_generic($td, $str);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return base64_encode($encrypted);
    }

    public static function decrypt($code) {
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        mcrypt_generic_init($td, self::$key, self::hexToStr(self::$hex_iv));
        $str = mdecrypt_generic($td, base64_decode($code));
        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return self::strippadding($str);
    }

    /*
      For PKCS7 padding
     */
    private static function addpadding($string, $blocksize = 16) {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);
        return $string;
    }

    private static function strippadding($string) {
        $slast = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
        if (preg_match("/$slastc{" . $slast . "}/", $string)) {
            $string = substr($string, 0, strlen($string) - $slast);
            return $string;
        } else {
            return false;
        }
    }

    private static function hexToStr($hex)
    {
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2)
        {
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }

    public static function newEncrypt($str)
    {
        $encrypted = openssl_encrypt($str, 'aes-256-cbc', self::$key, 0, self::hexToStr(self::$hex_iv));
        return $encrypted;
    }

    public static function newDecrypt($str)
    {
        $decrypted = openssl_decrypt($str, 'aes-256-cbc', self::$key, 0, self::hexToStr(self::$hex_iv));
        return $decrypted;
    }
}