<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/21
 * Time: 11:09
 */

namespace App\Services\Crypt\Api;

use Illuminate\Support\Facades\Config;

class PHPJsAes
{
    public static function encrypt($data) {
        return openssl_encrypt($data, 'aes-256-cbc', hash('sha256', Config::get('api.aes_key_h5'), true), false, Config::get('api.iv_h5'));
    }

    public static function decrypt($data) {
        return openssl_decrypt($data, 'aes-256-cbc', hash('sha256', Config::get('api.aes_key_h5'), true), false, Config::get('api.iv_h5'));
    }
}