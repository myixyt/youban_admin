<?php
/**
 * Created by PhpStorm.
 * User: sdf_sky
 * Date: 2017/1/20
 * Time: 下午3:29
 */

namespace App\Services;


class EncryptionService
{
    private  $key = 'aWH49UHzZEnUXzXVa9/ijQN3WzZzfpgOFbbqWP92DJA=';
    private  $iv = 'WL7KdgEBBWvkdkeUFAA4bQ==';

    public function __construct()
    {
        $config = config('app.encryption',[]);
        if($config){
            $this->key = $config['key'];
            $this->iv  =  $config['iv'];
        }
    }


    public function encrypt($string){
        return base64_encode(openssl_encrypt($string, 'aes-256-cbc', base64_decode($this->key), OPENSSL_RAW_DATA, base64_decode($this->iv)));
    }

    public function decrypt($string){
        return openssl_decrypt(base64_decode($string), 'aes-256-cbc', base64_decode($this->key), OPENSSL_RAW_DATA, base64_decode($this->iv));
    }


}