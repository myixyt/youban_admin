<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/14
 * Time: 13:51
 */

namespace App\Services;


use Illuminate\Support\Facades\Storage;

class ImageService
{
    /**
     * 获取图片二进制
     * @param $path
     * @return bool|string
     */
    public static function getImage($path)
    {
        if (!Storage::exists($path)) {
            return false;
        }
        $content = Storage::get($path);
        return $content;
    }

    /**
     * 删除图片
     * @param $path
     * @return bool
     */
    public static function delImage($path)
    {
        if(!Storage::exists($path)){
            return false;
        }
        $del = Storage::delete($path);
        return $del;
    }

    /**
     * 获取图片扩展名
     * @param $file
     * @return string
     */
    public static function getExtension($file)
    {
        return pathinfo($file, PATHINFO_EXTENSION);

    }
}