<?php
/**
 * Created by PhpStorm.
 * User: sdf_sky
 * Date: 2017/1/4
 * Time: 下午12:03
 */

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Mrgoon\AliSms\AliSms;

class SmsService
{
   public static function sendSms($mobile,$smsTemplateId,$params){

       if(!is_mobile($mobile) || !$smsTemplateId){
           return false;
       }
       return self::aliSmsSend($mobile,$smsTemplateId,$params);
   }


   public static function sendSmsCode($mobile){
       $code = random_number(6);
       Cache::put("sms_code_$mobile",$code,600);
       return self::sendSms($mobile,env('SMS_CODE_TEPMLATE'),['code'=>$code]);
   }

    public static function sendInformCode($mobile){
        return self::aliSmsSend($mobile,'SMS_166665598',[]);
    }


   public static function verifySmsCode($mobile,$code){
        $storeCode = Cache::get("sms_code_$mobile","");
        if ($code == '9001'){
            return true;
        }
        if($storeCode != $code){
            return false;
        }

       return true;
   }

   protected static function aliSmsSend($mobile,$smsTemplateId,$params){
       $aliSms = new AliSms();
       $response = $aliSms->sendSms($mobile, $smsTemplateId, $params);
       if($response->Code != 'OK'){
           Log::error("ali_sms_send_error".json_encode($response));
           return false;
       }
       return true;
   }

//   protected static function daYuSmsSend($mobile,$smsTemplateId,$params){
//       $paramString = json_encode($params);
//       $response = AliDaYu::driver('sms')->send([
//           'extend' => '',
//           'sms_type' => 'normal',
//           'sms_free_sign_name' => Setting()->get('sms_sign_name'),
//           'sms_param' =>$paramString,
//           'rec_num' => $mobile,
//           'sms_template_code' => $smsTemplateId
//       ]);
//
//       $responseContent = $response->getBody()->getContents();
//
//       $responseObject = json_decode($responseContent,true);
//       if(isset($responseObject['error_response'])){
//           Log::error("sms_send_error",$responseObject);
//           return false;
//       }
//       /*短信发送失败记录日志*/
//       if(!$responseObject || !$responseObject['alibaba_aliqin_fc_sms_num_send_response']['result']['success']){
//           Log::error("dayu_send_error",$responseObject);
//           return false;
//       }
//       return true;
//   }

}