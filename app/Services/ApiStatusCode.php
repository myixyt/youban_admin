<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/1
 * Time: 10:58
 */

namespace App\Services;


class ApiStatusCode
{
    const OK = 0;

    const PARAMS_LACK = 102;
    const DECRYPTION_FAILED = 103;
    const REPLAY_TIME_EXCEEDED = 104;
    const DUPLICATED_REQUEST = 105;
    const INVALID_SIGN = 106;
    const INVALID_CLIENT = 107;

    // 接口调用过于频繁
    const RATE_LIMIT_EXCEEDED = 429;

    const FILE_UPLOAD_ERROR = 205;

    const INVALID_AUTH_TOKEN = 402;

    const CANT_PROCESS_ENTITY = 422;

    const RESOURCE_ALREADY_EXISTS = 409;
    const RESOURCE_CREATE_FAILED = 423;

    const RESOURCE_COLLISION = 424;

    const RESOURCE_NOT_FOUND = 404;
    const RESOURCE_ACCESS_DENIED = 410;
    const RESOURCE_STATUS_LOCK_FAIL = 411;
    const RESOURCE_LACK_PARAMS = 420;

    const LOGIN_FAILED = 501;
    const LOGIN_SUCCESS = 502;

    const SEND_SMS_CODE_FAILED = 202;
    const VERIFY_SMS_CODE_FAILED = 204;
    const MOBILE_ALREADY_EXISTS = 205;
    const WX_ALREADY_BIND = 206;
    const WX_ALREADY_BEEN_BIND = 207;
    const ALREADY_BIND_INVITATION_CODE = 208;
    const INVITATION_CODE_UN_EXISTS = 209;
    const INVALID_INVITATION_CODE = 210;

    static $verify = [
        self::INVALID_AUTH_TOKEN => '无效的token',
        self::MOBILE_ALREADY_EXISTS => '手机号码已存在！',
        self::WX_ALREADY_BIND => '微信已绑定！',
        self::WX_ALREADY_BEEN_BIND => '该微信账户已存在！',
        self::ALREADY_BIND_INVITATION_CODE => '您已绑定邀请码！',
        self::INVITATION_CODE_UN_EXISTS => '邀请码不存在',
        self::INVALID_INVITATION_CODE => '邀请码不可用',
    ];

    //登陆、注册相关
    const ADD_USER_FAILED = 801;
    const ADD_TOKEN_FAILED = 802;
    const LOG_OUT_FAILED = 803;
    const INVALID_TOKEN = 804;
    const ILLEGAL_TOKEN = 805;
    const REGISTER_DEVICE_FAILED = 806;
    const USER_UN_EXISTS = 807;
    const PASSWORD_ERROR = 808;
    const REGISTER_FAILED = 809;
    const EMAIL_EXISTS = 810;
    const INVALID_URL = 811;
    const ILLEGAL_URL = 812;
    const NO_AVAILABLE_EMAIL = 813;
    const VERIFIED_EMAIL = 814;
    const INVALID_EMAIL = 815;
    const UPDATE_PROFILE_FAILED = 816;
    const NO_ALREADY_EXIT = 817;

    // 资源
    const SOURCE_NO_EXIT = 911;
    const CONTAIN_SENSITIVE_WORDS=912;
    const ILLEGAL_OPERATION = 913;
    static $source = [
        self::SOURCE_NO_EXIT => '资源不存在',
        self::ILLEGAL_OPERATION => '非法操作',
    ];

    static $login = [
        self::LOGIN_FAILED => '登录失败',
        self::LOGIN_SUCCESS => '登录成功',
        self::ADD_USER_FAILED => ' 添加用户失败',
        self::ADD_TOKEN_FAILED => 'token添加失败',
        self::LOG_OUT_FAILED => '退出登录失败',
        self::INVALID_TOKEN => '无效的token',
        self::ILLEGAL_TOKEN => '非法的token',
        self::REGISTER_DEVICE_FAILED => '设备注册失败',
        self::USER_UN_EXISTS => '用户不存在',
        self::PASSWORD_ERROR => '密码错误',
        self::REGISTER_FAILED => '注册用户失败',
        self::EMAIL_EXISTS => '邮箱已被使用',
        self::INVALID_URL => 'Invalid link.',
        self::ILLEGAL_URL => 'Illegal link.',
        self::NO_AVAILABLE_EMAIL => 'No available email.',
        self::VERIFIED_EMAIL => 'This mailbox has been verified.',
        self::INVALID_EMAIL => 'This email is invalid.',
        self::VERIFY_SMS_CODE_FAILED => '验证码无效',
    ];

    const OPERATE_FAILED = 601;

    const IMAGE_UN_EXISTS = 700;
    static $user = [
        self::IMAGE_UN_EXISTS => '图片不存在',
        self::SEND_SMS_CODE_FAILED => '发送短信验证码失败',
        self::VERIFY_SMS_CODE_FAILED => '验证码过期或不存在',
        self::UPDATE_PROFILE_FAILED => '修改个人信息失败,请重试',
        self::USER_UN_EXISTS => '用户不存在',
    ];
}