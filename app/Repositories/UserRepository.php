<?php
/**
 * Created by PhpStorm.
 * User: sdf_sky
 * Date: 2019/3/18
 * Time: 4:43 PM
 */

namespace App\Repositories;


use App\Models\Question;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

class UserRepository
{
    protected $user;
    protected $auth;

    public function __construct(User $user, Guard $auth)
    {
        $this->user = $user;
        $this->auth = $auth;
    }

    public function loginAndRegister($mobile)
    {
        $data = [
            'mobile'   => $mobile,
            'username' => $mobile,
            'password' => bcrypt('123456'),
            'status'   => 1,
        ];
        $user = $this->user->where('mobile', $mobile)->first();
        if (empty($user)) {
            $user = $this->user->create($data);
        }
        return $user;
    }

    // api登录注册
    public function apiLogin($data)
    {
        $user = $this->user->where('mobile', $data['mobile'])->first();
        if (empty($user)) {
            $data['role'] = 2;
            $user = $this->user->create($data);

            // 绑定医生扫码量加1
            User::where('id',$data['bind_user'])->increment('views');
            // 生成、更新token
            $token = $this->loginNext($user->id);
            $user->token = $token;
        } else {
            $user->token = $this->loginNext($user->id);
            $user->bind_user = $data['bind_user'];
            $user->save();
        }
        $user = $this->user->where('mobile', $data['mobile'])->first();
        return $user;
    }

    // 登录
    public function login($mobile)
    {
        $user = $this->user->where('mobile', $mobile)->first();
        $user->token = $this->loginNext($user->id);
        $user->save();
        return $user;
    }


    /**
     * 根据token获取用户信息
     * @param $token
     * @return mixed
     */
    public function getUserInfoByToken($token)
    {
        return $this->user->where(['token' => $token])->first();
    }

    // 生成token
    public function loginNext($userId)
    {
        if (empty($userId)) {
            return false;
        }
        //生成token
        $token = $this->buildToken($userId);
        //添加token
        $this->user->where(['id' => $userId])->update(['token' => $token]);
        return $token;
    }

    /**
     * 生成token
     * @param $userId
     * @return string
     */
    public function buildToken($userId)
    {
        $time = microtime(true);
        $rand1 = rand(100000, 999999);
        $rand2 = rand($rand1, $time);
        return base64_encode(md5($time . $rand1 . $rand2) . base64_encode(Config::get('api.token_prefix') . $userId));
    }

    /**
     * 解析token
     * @param $token
     * @return int|string
     */
    public function decodeToken($token)
    {
        if (empty($token)) return 0;
        $str = base64_decode($token);
        $second = base64_decode(substr($str, 32));
        return substr($second, strlen(Config::get('api.token_prefix')));
    }

    public function getPatients($user_id, $page = 1, $pageSize = 10)
    {
        $offset = ($page - 1) * $pageSize;
        return $this->user->where('bind_user', $user_id)->offset($offset)->limit($pageSize)->get();
    }

    // 问题列表
    public function questions($user_id, $page = 1, $pageSize = 10)
    {
        $offset = ($page - 1) * $pageSize;
        $lists = Question::where('to_user_id', $user_id)->offset($offset)->limit($pageSize)->orderBy('created_at', 'desc')->get();
        $lists->map(function ($list) {
            $list->create_at = dateStr(strtotime($list->created_at));
            $list->cycle_desc = json_decode($list->cycle_desc);
            $list->expression = json_decode($list->expression);
        });
        return $lists;
    }

}