<?php
/**
 * Created by PhpStorm.
 * User: sdf_sky
 * Date: 2019/3/18
 * Time: 4:43 PM
 */

namespace App\Repositories;


use App\Models\Question;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

class UsersRepository
{
    /**
     * 根据token获取用户信息
     * @param $token
     * @return mixed
     */
    public function getUserInfoByToken($token)
    {
        return User::where(['token' => $token])->first();
    }

    // 生成token
    public function loginNext($userId)
    {
        if (empty($userId)) {
            return false;
        }
        //生成token
        $token = $this->buildToken($userId);
        //添加token
        User::where(['id' => $userId])->update(['token' => $token]);
        return $token;
    }

    /**
     * 生成token
     * @param $userId
     * @return string
     */
    public function buildToken($userId)
    {
        $time = microtime(true);
        $rand1 = rand(100000, 999999);
        $rand2 = rand($rand1, $time);
        return base64_encode(md5($time . $rand1 . $rand2) . base64_encode(Config::get('api.token_prefix') . $userId));
    }

    /**
     * 解析token
     * @param $token
     * @return int|string
     */
    public function decodeToken($token)
    {
        if (empty($token)) return 0;
        $str = base64_decode($token);
        $second = base64_decode(substr($str, 32));
        return substr($second, strlen(Config::get('api.token_prefix')));
    }

}