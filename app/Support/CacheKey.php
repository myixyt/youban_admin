<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/1
 * Time: 11:41
 */

namespace App\Support;


class CacheKey
{
    public static function getSmsQueueKey()
    {
        return 'shuke' . config('app.env') . ':queue:sms';
    }

    public static function getCompleteContractQueueKey()
    {
        return 'shuke' . config('app.env') . ':queue:completecontract';
    }

    public static function getArchiveContractQueueKey()
    {
        return 'shuke' . config('app.env') . ':queue:archivecontract';
    }

    public static function getCreateContractQueueKey()
    {
        return self::getPrefix() . ':queue:createcontract';
    }

    public static function getTokenCacheKey()
    {
        return self::getPrefix() . ':token';
    }

    public static function getSmsCacheKey()
    {
        return self::getPrefix() . ':sms_code';
    }

    public static function getNonceCacheKey($nonce)
    {
        return self::getPrefix() . ':nonce:' . $nonce;
    }

    private static function getPrefix()
    {
        return 'shuke' . config('app.env');
    }

    public static function getInsEmpTokenPre()
    {
        return 'shuke_user';
    }
}