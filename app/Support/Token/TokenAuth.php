<?php
/**
 * Created by PhpStorm.
 * User: lxl
 * Date: 2017/5/18
 * Time: 0:33
 */

namespace App\Support\Token;


use App\Exceptions\Api\ApiException;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class TokenAuth
{
    private $manager;

    private $request;

    private $token;

    public function __construct()
    {
        $this->manager = new TokenManager();
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function parseToken()
    {
        // get token and set it.
        $tokenStr = $this->request->input('token');
        $this->token = new SimpleToken($tokenStr);

        return $this;
    }

    public function toUser($token = false)
    {
        $payload = $this->getPayload($token);

        $userRepo = new UserRepository();
        return $userRepo->getById($payload->customer_id);
    }

    public function getPayload($token = false)
    {
        $this->requireToken($token);
        $secret = env('APP_KEY');

        return $this->manager->decode($this->token, $secret);
    }

    public function requireToken($token = false)
    {
        if ($token) {
            return $this->setToken($token);
        } elseif ($this->token) {
            return $this;
        } else {
            throw new ApiException('403', 'token为必须参数');
        }
    }

    public function setToken($token)
    {
        $this->token = new SimpleToken($token);
        return $this;
    }
}