<?php
/**
 * Created by PhpStorm.
 * User: lxl
 * Date: 2017/5/18
 * Time: 0:33
 */

namespace App\Support\Token;


use App\Exceptions\Api\ApiException;
use App\Services\ApiStatusCode;

class TokenManager
{
    public function encode(array $payloads, $secretKey, $alg = 'HS256')
    {
        $header = ['type' => 'stk', 'alg' => $alg];

        $segments = [];
        $segments[] = $this->base64UrlSafeEncode(json_encode($header));
        $segments[] = $this->base64UrlSafeEncode(json_encode($payloads));

        $strToSign = implode('.', $segments);
        $segments[] = $this->base64UrlSafeEncode($this->sign($strToSign, $secretKey, $alg));

        return implode('.', $segments);
    }

    public function decode(SimpleToken $token, $secret, $verify = true)
    {
        // verify token
        $tokens = explode('.', $token->get());

        list($headerBase64, $payloadBase64, $cryptoBase64) = $tokens;
        
        $header = json_decode($this->base64UrlSafeDecode($headerBase64));
        $payload = json_decode($this->base64UrlSafeDecode($payloadBase64));

        if (null == $header || null == $payload) {
            throw new ApiException(ApiStatusCode::INVALID_AUTH_TOKEN, ApiStatusCode::$verify[ApiStatusCode::INVALID_AUTH_TOKEN]);
        }

        $sig = $this->base64UrlSafeDecode($cryptoBase64);
        if ($verify) {
            $verifySig = $this->sign("{$headerBase64}.{$payloadBase64}", $secret, $header->alg);
            if ($sig != $verifySig) {
                throw new ApiException(ApiStatusCode::INVALID_AUTH_TOKEN, ApiStatusCode::$verify[ApiStatusCode::INVALID_AUTH_TOKEN]);
            }
        }

        return $payload;
    }

    private function sign($str, $secretKey, $alg = 'HS256')
    {
        $algorithms = array(
            'HS256' => 'sha256',
            'HS384' => 'sha384',
            'HS512' => 'sha512',
        );
        if (empty($algorithms[$alg])) {
            throw new ApiException(ApiStatusCode::INVALID_AUTH_TOKEN, ApiStatusCode::$verify[ApiStatusCode::INVALID_AUTH_TOKEN]);
        }
        return hash_hmac($algorithms[$alg], $str, $secretKey, true);
    }

    public function base64UrlSafeEncode($str)
    {
        return str_replace('=', '', strtr(base64_encode($str), '+/', '-_'));
    }

    public function base64UrlSafeDecode($str)
    {
        $remainder = strlen($str) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $str .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($str, '-_', '+/'));
    }
}