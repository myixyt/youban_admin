<?php
/**
 * Created by PhpStorm.
 * User: lxl
 * Date: 2017/10/13
 * Time: 16:51
 */

namespace App\Support\Token;

use App\Support\CacheKey;
use Illuminate\Support\Facades\Redis;

class TokenIssuer
{
    public static function issueToken(array $payloads)
    {
        $secret = env('APP_KEY');
        $tokenManager = new TokenManager();

        $payloads['iat'] = microtime();
        $token = $tokenManager->encode($payloads, $secret);
        return $token;
    }

    // 604800 token缓存30天
    public static function cacheToken($identifier, $token, $ttl = 2592000)
    {
        $cacheKey = self::getCacheKey($identifier);
        Redis::setex($cacheKey, $ttl, $token);
    }

    public static function getToken($identifier)
    {
        $cacheKey = self::getCacheKey($identifier);
        return Redis::get($cacheKey);
    }

    public static function getCacheKey($key)
    {
        return CacheKey::getTokenCacheKey() . ":{$key}";
    }

    public static function revokeToken($identifier)
    {
        $cacheKey = self::getCacheKey($identifier);
        return Redis::del($cacheKey);
    }
}
