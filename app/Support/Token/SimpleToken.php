<?php
/**
 * Created by PhpStorm.
 * User: lxl
 * Date: 2017/5/17
 * Time: 23:38
 */

namespace App\Support\Token;

use App\Exceptions\Api\ApiException;
use App\Services\ApiStatusCode;

class SimpleToken
{
    private $value;

    public function __construct($value)
    {
        if (!with(new TokenValidator())->check($value)) {
            throw new ApiException(ApiStatusCode::INVALID_AUTH_TOKEN, ApiStatusCode::$verify[ApiStatusCode::INVALID_AUTH_TOKEN]);
        }
        $this->value = $value;
    }

    public function get()
    {
        return $this->value;
    }

    public function __toString()
    {
        return (string)$this->value;
    }
}