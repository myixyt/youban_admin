<?php
/**
 * Created by PhpStorm.
 * User: lxl
 * Date: 2017/5/17
 * Time: 23:38
 */

namespace App\Support\Token;


class TokenValidator
{
    public function check($value)
    {
        return $this->validateStructure($value);
    }

    protected function validateStructure($value)
    {
        return 3 == count(explode('.', $value));
    }
}