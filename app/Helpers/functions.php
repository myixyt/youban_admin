<?php
/**
 * User: ${yangman}
 * Date: 2018/8/24
 * Time: 下午12:33
 */

/*手机号码判断*/
if ( !function_exists('is_mobile')) {
    function is_mobile($mobile)
    {
        $reg = "/^1[3456789]\d{9}$/";
        if ( !preg_match($reg, $mobile)) {
            return false;
        }
        return true;
    }
}

/*文章类型定义*/
if ( !function_exists('trans_article_type')) {

    function trans_article_type($type)
    {
        $map = [
            1 => '妇科讲堂',
            2 => '痛经常识',
            3 => '如何治疗',
            4 => '护理指导',
            9 => '未定义'
        ];

        if ($type === 'all') {
            return $map;
        }

        if (isset($map[$type])) {
            return $map[$type];
        }
        return '';
    }
}

/*文章类型定义*/
if ( !function_exists('trans_user_role')) {

    function trans_user_role($type)
    {
        $map = [
            1 => '医生',
            2 => '患者',
        ];

        if ($type === 'all') {
            return $map;
        }

        if (isset($map[$type])) {
            return $map[$type];
        }
        return '';
    }
}

/*文章类型定义*/
if ( !function_exists('trans_article_status')) {

    function trans_article_status($status)
    {
        $map = [
            4 => '下线',
            1 => '上线',
        ];

        if ($status === 'all') {
            return $map;
        }
        if (isset($map[$status])) {
            return $map[$status];
        }
        return '';
    }
}

/*所有专家*/
if ( !function_exists('load_experts')) {

    function load_experts()
    {
        return app('App\Models\Expert')->loadExperts();
    }
}


if ( !function_exists('build_user_no')) {
    function build_user_no()
    {
        $order_no = substr(implode(null, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 9);
        return $order_no;
    }
}

/*创建唯一订单号*/
if (!function_exists('random_number')) {
    function random_number($length = 6)
    {
        $pool = '0123456789';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }
}

/*文章类型定义*/
if ( !function_exists('trans_sex_type')) {

    function trans_sex_type($status)
    {
        $map = [
            1 => '女',
            2 => '男',
        ];

        if ($status === 'all') {
            return $map;
        }
        if (isset($map[$status])) {
            return $map[$status];
        }
        return '';
    }
}

function dateStr($date){
    if((time()-$date)<60*10){
        //十分钟内
        return '刚刚';
    }elseif(((time()-$date)<60*60)&&((time()-$date)>=60*10)){
        //超过十分钟少于1小时
        $s = floor((time()-$date)/60);
        return  $s."分钟前";
    }elseif(((time()-$date)<60*60*24)&&((time()-$date)>=60*60)){
        //超过1小时少于24小时
        $s = floor((time()-$date)/60/60);
        return  $s."小时前";
    }elseif(((time()-$date)<60*60*24*3)&&((time()-$date)>=60*60*24)){
        //超过1天少于3天内
        $s = floor((time()-$date)/60/60/24);
        return $s."天前";
    }else{
        //超过3天
        return  date("Y/m/d",$date);
    }
}


/*回复类型定义*/
if ( !function_exists('trans_reply_status')) {

    function trans_reply_status($status)
    {
        $map = [
            0 => '未回复',
            1 => '已回复',
        ];

        if ($status === 'all') {
            return $map;
        }
        if (isset($map[$status])) {
            return $map[$status];
        }
        return '';
    }
}

/*回复类型定义*/
if ( !function_exists('trans_deal_status')) {

    function trans_deal_status($status)
    {
        $map = [
            0 => '未处理',
            1 => '已处理',
        ];

        if ($status === 'all') {
            return $map;
        }
        if (isset($map[$status])) {
            return $map[$status];
        }
        return '';
    }
}
//
if ( !function_exists('trans_common_status')) {

    function trans_common_status($status)
    {
        $map = [
            1 => '上线',
            0 => '下线',
        ];

        if ($status === 'all') {
            return $map;
        }
        if (isset($map[$status])) {
            return $map[$status];
        }
        return '';
    }
}
