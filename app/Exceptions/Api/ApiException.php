<?php

namespace App\Exceptions\Api;

/**
 * 自定义异常。
 * Class CustomException
 * @package App\Exceptions
 */
class ApiException extends \Exception
{
    /**
     * 自定义状态码，非HTTP状态码
     * @var
     */
    protected $status;

    /**
     * 返回数据
     * @var
     */
    protected $data;

    /**
     * 错误消息
     * @var
     */
    protected $msg;

    /**
     * API 响应码
     * @var string
     */
    protected $code;

    protected $httpStatusCode = 200;

    public function __construct($code, $message = '', $data = [])
    {
        parent::__construct($message);
        $this->code = $code;
        $this->msg = $message;
        $this->data = $data;
    }

    public function toArray()
    {
        return [
            'code' => $this->code,
            'data' => $this->data,
            'msg' => $this->msg,
        ];
    }

    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }
}