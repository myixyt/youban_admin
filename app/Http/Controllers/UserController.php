<?php
/**
 * Created by PhpStorm.
 * User: sdf_sky
 * Date: 2019/3/18
 * Time: 3:35 PM
 */

namespace App\Http\Controllers;


use App\Repositories\SmsRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function login(Request $request,SmsRepository $smsRepository,UserRepository $userRepository)
    {
        if($request->isMethod('post')){
            $code = $request->input('code');
            $mobile = $request->input('mobile');
            $this->validate($request, [
                'mobile' => 'required|regex:/^1[34578]\d{9}$/',
                'code' => 'required|min:4|max:6',
            ],[
                'mobile.required' => '手机号码格式不正确',
                'mobile.regex' => '手机号码格式不正确',
                'code.required' => '验证码不能为空',
            ]);

            if($smsRepository->verifySmsCode($mobile,$code)){
                $user = $userRepository->loginAndRegister($mobile);
                Auth('web')->loginUsingId($user->id);
                return redirect()->intended();
            }
            $request->flash();
           return view('login')->withErrors([
               'code' => '验证码输入错误，请核实'
           ]);
        }
        return view('login');
    }

    public function sendSmsCode(Request $request, SmsRepository $smsRepository)
    {
        $mobile = $request->input('mobile', '');
        if (!is_mobile($mobile)) {
            return $this->ajaxError(10004, '手机号格式码错误');
        }
        /*次数限制*/
        $sendTimes = $this->counter('send_sms_counter_' . $mobile);
        if ($sendTimes > config('zhanqun.sms_limit_times', 10)) {
            return $this->ajaxError(10005, '短信验证码发送数量已超出当日限制，请明天再试');
        }

        if (!$smsRepository->sendSmsCode($mobile)) {
            return $this->ajaxError(10006, '短信发送过于频繁，请稍后再试');
        }
        $this->counter('send_sms_counter_' . $mobile, 1);
        return $this->ajaxSuccess("success");
    }
}