<?php
/**
 * Created by PhpStorm.
 * User: sdf_sky
 * Date: 2019/3/18
 * Time: 3:26 PM
 */

namespace App\Http\Controllers;


use App\Models\Product;
use App\Repositories\ProductRepository;

class ProductController extends Controller
{

    public function show($id,ProductRepository $productRepository)
    {
        $product = Product::findOrFail($id);
        $productRepository->click($id);
        return redirect()->to($product->url);
    }

}