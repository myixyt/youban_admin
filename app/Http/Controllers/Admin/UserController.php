<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends AdminController
{
    protected $rules = [
        'title'    => 'required',
        'nickname' => 'required',
        'wechat'   => 'required',
        'content'  => 'required',
        'mobile'   => 'required|integer',
        'age'      => 'required|integer',
    ];
    protected $message = [
        'title.required'    => '职称不能为空',
        'nickname.required' => '姓名不能为空',
        'wechat.required'   => '微信不能为空',
        'content.required'  => '医生简介不能为空',
        'mobile.required'   => '医生手机号不能为空',
        'age.required'      => '医生年龄不能为空',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->all();

        $query = User::query();

        // 管理员判断
        $admin_id = auth()->user()->id;
        if ($admin_id > 1){
            $query->where('admin_id',$admin_id);
        }

        /*手机号过滤*/
        if (isset($filter['mobile']) && $filter['mobile'] > 0) {
            $query->where('mobile', '=', $filter['mobile']);
        }

        /*类型过滤*/
        if (isset($filter['role']) && $filter['role'] > 0) {
            $query->where('role', '=', $filter['role']);
        }

        /*时间过滤*/
        if (isset($filter['date_range']) && $filter['date_range']) {
            $query->whereBetween('created_at', explode(" - ", $filter['date_range']));
        }

        $users = $query->orderBy('created_at', 'desc')->paginate(20);
        return view("admin.user.index")->with('users', $users)->with('filter', $filter);
    }


    public function add()
    {
        return view('admin.user.add');
    }

    /**
     * 添加文章
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->message);

        if ($validator->fails()) {
            return redirect(route('admin.user.add'))->withErrors($validator)->withInput();
        }
        $input = $request->only(['title', 'nickname', 'status', 'content', 'wechat','age','mobile','sex']);
        $input['youzan_url'] = $request->input('youzan_url') ?? '';
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'users/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['avatar'] = str_replace('/', '-', $filePath);
        }
        $input['role'] = 1;
        $input['admin_id'] = auth()->user()->id;
        $result = User::create($input);
        return $this->success(route('admin.user.index'), '添加成功！');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        if (empty($user)) {
            abort(404);
        }
        return view('admin.user.edit')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
//        $validator = Validator::make($request->all(), $this->rules, $this->message);
//
//        if ($validator->fails()) {
//            return redirect(route('admin.user.edit',['id'=>$id]))->withErrors($validator)->withInput();
//        }
        $input = $request->only(['title', 'nickname', 'status', 'content', 'wechat','age','mobile','sex','note']);
        $input['youzan_url'] = $request->input('youzan_url') ?? '';
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'articles/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['avatar'] = str_replace('/', '-', $filePath);
        }
        $result = User::where('id', $id)->update($input);
        return $this->success(route('admin.user.index'), '编辑成功！');
    }


    /**
     * 删除文章
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ids = $request->input('id');
        User::destroy($ids);
        return $this->success(route('admin.user.index'), '删除成功');
    }
}
