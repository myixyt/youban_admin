<?php

namespace App\Http\Controllers\Admin;

use App\Models\Activity;
use App\Models\Expert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ActivityController extends AdminController
{
    protected $rules = [
        'topic'  => 'required',
        'sort'   => 'sometimes|integer',
        'status' => 'sometimes|integer',
    ];
    protected $message = [
        'topic.required' => '话题不能为空',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Activity::query();

        $activities = $query->orderBy('created_at', 'desc')->paginate(20);
        return view("admin.activity.index")->with('activities', $activities);
    }


    public function add()
    {
        return view('admin.activity.add');
    }

    /**
     * 添加
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->message);

        if ($validator->fails()) {
            return redirect('admin/activity/add')->withErrors($validator)->withInput();
        }
        $input = $request->only(['topic', 'preferred_date', 'preferred_time', 'address', 'sort', 'status', 'experts', 'live_url', 'url', 'apply_url']);
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'experts/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['logo'] = str_replace('/', '-', $filePath);
        }
        $input = array_filter($input);
        if (isset($input['experts'])) {
            $input['experts'] = implode(',', $input['experts']);
        }
        $result = Activity::create($input);
        return $this->success(route('admin.activity.index'), '添加成功！');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $activity = Activity::find($id);
        if (empty($activity)) {
            abort(404);
        }
        $activity['experts'] = explode(',', $activity['experts']);
        return view('admin.activity.edit')->with(compact('activity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), $this->rules, $this->message);

        if ($validator->fails()) {
            return redirect(route('admin.activity.edit', ['id' => $id]))->withErrors($validator)->withInput();
        }
        $input = $request->only(['topic', 'preferred_date', 'preferred_time', 'address', 'sort', 'status', 'experts', 'live_url', 'url', 'apply_url']);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'experts/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['logo'] = str_replace('/', '-', $filePath);
        }
        $input = array_filter($input);
        if (isset($input['experts'])) {
            $input['experts'] = implode(',', $input['experts']);
        } else {
            $input['experts'] = '';
        }
        $result = Activity::where('id', $id)->update($input);
        return $this->success(route('admin.activity.index'), '编辑成功！');
    }


    /**
     * 删除
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');
        Activity::destroy($id);
        return $this->success(route('admin.activity.index'), '删除成功');
    }

    public function setCurrent(Request $request)
    {
        $id = $request->input('id');
        Activity::where('id', $id)->update(['is_current' => 1]);
        Activity::where('id', '<>', $id)->update(['is_current' => 0]);
        return;
    }
}
