<?php

namespace App\Http\Controllers\Admin;

use App\Models\Experience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ExperienceController extends AdminController
{
    protected $rules = [
        'username' => 'required',
        'age'      => 'required|integer',
        'district' => 'required',
        'keywords' => 'required',
        'disease'  => 'required',
        'content'  => 'required',
    ];
    protected $message = [
        'username.required' => '称呼不能为空',
        'age.required'      => '年龄不能为空',
        'district.required' => '地区不能为空',
        'disease.required'  => '病种不能为空',
        'content.required'  => '内容不能为空',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->all();

        $query = Experience::query();

        /*类型过滤*/
        if (isset($filter['status']) && $filter['status'] > 0) {
            $query->where('status', '=', $filter['status']);
        }

        /*昵称过滤*/
        if (isset($filter['username']) && $filter['username']) {
            $query->where('username', 'like', '%' . $filter['username'] . '%');
        }

        /*时间过滤*/
        if (isset($filter['date_range']) && $filter['date_range']) {
            $query->whereBetween('created_at', explode(" - ", $filter['date_range']));
        }

        $experiences = $query->orderBy('created_at', 'desc')->paginate(20);
        return view("admin.experience.index")->with('experiences', $experiences)->with('filter', $filter);
    }


    public function add()
    {
        return view('admin.experience.add');
    }

    /**
     * 添加
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->message);

        if ($validator->fails()) {
            return redirect('admin/experience/add')->withErrors($validator)->withInput();
        }
        $input = $request->only(['username', 'age', 'district', 'keywords', 'disease', 'content', 'status', 'sort','tel','gender','title']);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'articles/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['logo'] = str_replace('/', '-', $filePath);
        }
        $input = array_filter($input);
        $result = Experience::create($input);
        return $this->success(route('admin.experience.index'), '添加成功！');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $experience = Experience::find($id);
        if (empty($experience)) {
            abort(404);
        }
        return view('admin.experience.edit')->with(compact('experience'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), $this->rules, $this->message);

        if ($validator->fails()) {
            return redirect(route('admin.experience.edit', ['id' => $id]))->withErrors($validator)->withInput();
        }
        $input = $request->only(['username', 'age', 'district', 'keywords', 'disease', 'content', 'status', 'sort','tel','gender','title']);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'articles/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['logo'] = str_replace('/', '-', $filePath);
        }
        $input = array_filter($input);
        $result = Experience::where('id', $id)->update($input);
        return $this->success(route('admin.experience.index'), '编辑成功！');
    }


    /**
     * 删除
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');
        Experience::destroy($id);
        return $this->success(route('admin.experience.index'), '删除成功');
    }
}
