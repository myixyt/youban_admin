<?php
/**
 * Created by PhpStorm.
 * User: nayo
 * Date: 2018/8/8
 * Time: 下午4:21
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImageController extends AdminController
{

    public function upload(Request $request){
         if($request->hasFile('file')){
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'books/'.gmdate("Y")."/".gmdate("m")."/".uniqid(str_random(8)).'.'.$extension;
            Storage::disk('local')->put($filePath,File::get($file));
             return response($filePath);
           //  return response(route("admin.image.show",['image_name'=>$filePath]));
            //return response(route("admin.image.show",['image_name'=>str_replace("/","-",$filePath)]));
           // return response(str_replace("/","-",$filePath));
        }
        return response('error');
    }
    //编辑器上传图片
    public function ueUpload(Request $request){
        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'attachments/'.gmdate("Y")."/".gmdate("m")."/".uniqid(str_random(8)).'.'.$extension;
            Storage::disk('local')->put($filePath,File::get($file));
            //return response(route("admin.image.show",['image_name'=>str_replace("/","-",$filePath)]));
            return res(['path' => str_replace("/","-",$filePath),'url'=>route('admin.image.show',['image_name'=>$filePath])]);
           // return res(['path' => $filePath,'url'=>route('admin.image.show',['image_name'=>$filePath])]);
        }
        return response('error');
    }



    public function showImage($image_name)
    {
        $imageFile = storage_path('app/'.str_replace("-","/",$image_name));
        if(!is_file($imageFile)){
            abort(404);
        }
        $image =   Image::make($imageFile);
        $response = response()->make($image->encode('jpg'));
        $response->header('Content-Type', 'image/jpeg');
        $response->header('Expires',  date(DATE_RFC822,strtotime(" 7 day")));
        $response->header('Cache-Control', 'private, max-age=259200, pre-check=259200');
        return $response;
    }

    /**
     * 获取图片
     * @param NewsRequest $request
     * @return mixed
     * @throws \Exception
     */
    public function getImage(Request $request)
    {
        //获取一张图片
        //$path = 'images/orders/2017/08/29/es1504003976.jpg';
        $path = $request->input('path');
        $binaryContent = ImageService::getImage($path);
        if (!$binaryContent) {
            abort(404);
        }
        return response($binaryContent)->header('Content-type', 'image/' . ImageService::getExtension($path));
    }


}