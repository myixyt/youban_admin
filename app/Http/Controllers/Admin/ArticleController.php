<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ArticleController extends AdminController
{
    protected $rules = [
        'title'        => 'required',
        'type'         => 'required|integer',
        'content'      => 'required',
        'publish_date' => 'sometimes|date',
    ];
    protected $message = [
        'title.required'   => '文章名称不能为空',
        'type.required'    => '请选择文章类型',
        'content.required' => '文章内容不能为空',
        'publish_date.date' => '发布日期格式为 YYYY-MM-DD',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->all();

        $query = Article::query();

        /*类型过滤*/
        if (isset($filter['type']) && $filter['type'] > 0) {
            $query->where('type', '=', $filter['type']);
        }

        /*标题过滤*/
        if (isset($filter['title']) && $filter['title']) {
            $query->where('title', 'like', '%' . $filter['word'] . '%');
        }

        /*时间过滤*/
        if (isset($filter['date_range']) && $filter['date_range']) {
            $query->whereBetween('created_at', explode(" - ", $filter['date_range']));
        }

        $articles = $query->orderBy('created_at', 'desc')->paginate(20);
        return view("admin.article.index")->with('articles', $articles)->with('filter', $filter);
    }


    public function add()
    {
        return view('admin.article.add');
    }

    /**
     * 添加文章
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->message);

        if ($validator->fails()) {
            return redirect('admin/article/add')->withErrors($validator)->withInput();
        }
        $input = $request->only(['title', 'type', 'source', 'keywords', 'summary', 'content', 'status']);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'articles/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['logo'] = str_replace('/', '-', $filePath);
        }
        $input = array_filter($input);
        $result = Article::create($input);
        return $this->success(route('admin.article.index'), '添加文章成功！');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $article = Article::find($id);
        if (empty($article)) {
            abort(404);
        }
        return view('admin.article.edit')->with(compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), $this->rules, $this->message);

        if ($validator->fails()) {
            return redirect(route('admin.article.edit',['id'=>$id]))->withErrors($validator)->withInput();
        }
        $input = $request->only(['title', 'type', 'source', 'keywords', 'summary', 'content', 'status', 'publish_date']);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'articles/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['logo'] = str_replace('/', '-', $filePath);
        }
        $input = array_filter($input);
        $result = Article::where('id', $id)->update($input);
        return $this->success(route('admin.article.index'), '编辑文章成功！');
    }


    /**
     * 删除文章
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $articleIds = $request->input('id');
        Article::destroy($articleIds);
        return $this->success(route('admin.article.index'), '文章删除成功');
    }
}
