<?php

namespace App\Http\Controllers\Admin;

use App\Models\Application;
use App\Models\Experience;
use App\Models\Expert;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ApplicationController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->all();

        $query = Application::query();

        /*类型过滤*/
        if (isset($filter['status']) && $filter['status'] > 0) {
            $query->where('status', '=', $filter['status']);
        }

        /*昵称过滤*/
        if (isset($filter['mobile']) && $filter['mobile']) {
            $query->where('mobile', 'like', '%' . $filter['mobile'] . '%');
        }

        /*时间过滤*/
        if (isset($filter['date_range']) && $filter['date_range']) {
            $query->whereBetween('created_at', explode(" - ", $filter['date_range']));
        }

        $applications = $query->orderBy('created_at', 'desc')->paginate(20);
        return view("admin.application.index")->with(compact('filter','applications'));
    }

    public function setStatus(Request $request)
    {
        $input = $request->only(['id','status']);
        $status = $request->input(['status']);
        if ($status){
            $status = 0;
            $deal_time = null;
        }else{
            $status = 1;
            $deal_time = Carbon::now();
        }
        $result = Application::where('id',$input['id'])->update(['status'=>$status,'deal_time'=>$deal_time]);
        if (!$result) {
            return 0;
        }
        return 1;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function questions(Request $request)
    {
        $filter = $request->all();

        $query = Question::query();

        /*类型过滤*/
        if (isset($filter['status']) && $filter['status'] > 0) {
            $query->where('status', '=', $filter['status']);
        }

        /*昵称过滤*/
        if (isset($filter['mobile']) && $filter['mobile']) {
            $query->where('mobile', 'like', '%' . $filter['mobile'] . '%');
        }

        /*时间过滤*/
        if (isset($filter['date_range']) && $filter['date_range']) {
            $query->whereBetween('created_at', explode(" - ", $filter['date_range']));
        }

        $questions = $query->orderBy('created_at', 'desc')->paginate(20);
        return view("admin.application.questions")->with(compact('filter','questions'));
    }
}
