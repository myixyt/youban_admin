<?php
/**
 * Created by PhpStorm.
 * User: nayo
 * Date: 2018/8/8
 * Time: 下午4:46
 */

namespace App\Http\Controllers\Admin;


use App\Models\Admin;
use App\Models\Channel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdministratorController extends AdminController
{


    /**
     * 管理员列表
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->all();
        $query = Admin::query()->where('id', '>', 1);

        /*手机号过滤*/
        if (isset($filter['mobile'])) {
            $query->where('mobile', $filter['mobile']);
        }

        /*时间过滤*/
        if (isset($filter['date_range']) && $filter['date_range']) {
            $query->whereBetween('created_at', explode(" - ", $filter['date_range']));
        }

        $admins = $query->orderBy('created_at', 'desc')->paginate(20);
        return view("admin.administrator.index")->with('admins', $admins)->with('filter', $filter);
    }

    public function add()
    {
        return view('admin.administrator.add');
    }

    /**
     * 添加管理员
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $request->flush();
        if ($request->input('mobile')) {
            $input['mobile'] = $request->input('mobile');
        }
        $input['username'] = $request->input('username');
        $input['password'] = $request->input('password');
        $input['status'] = $request->input('status', 0);
        $validator = Validator::make($input, [
            'username'   => 'required|regex:/^[a-zA-Z0-9_-]{4,16}$/|unique:admins,username|min:6',
            'password'   => 'required',
            'status'     => 'required|in:0,1',
            'mobile'     => 'required|regex:/^1[34578]\d{9}$/',
        ], [
            'username.required' => '用户名不能为空',
            'username.unique'   => '用户名已存在',
            'mobile.regex'      => '手机号码格式不正确',
            'username.regex'    => '用户名必须完全是字母、数字',
            'password.required' => '密码不能为空',
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.admin.add'))->withErrors($validator)->withInput();
        }
        // 密码加密
        $input['password'] = bcrypt($input['password']);
        $result = Admin::create($input);
        return $this->success(route('admin.admin.index'), '添加成功！');
    }

    /**
     * 编辑页
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function edit(Request $request, $id)
    {
        $admin = Admin::find($id);
        return view('admin.administrator.edit')->with(compact('admin'));
    }

    /**
     * 修改
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function update(Request $request, $id)
    {
        if ($request->input('mobile')) {
            $input['mobile'] = $request->input('mobile');
        }
        $input['password'] = $request->input('password');
        $input['username'] = $request->input('username');
        $input['status'] = $request->input('status', 0);
        $validator = Validator::make($input, [
            'username'   => 'required|regex:/^[a-zA-Z0-9_-]{4,16}$/',
            'status'     => 'required|in:0,1',
            'mobile'     => 'sometimes|regex:/^1[34578]\d{9}$/',
        ], [
            'username.required' => '用户名不能为空',
            'username.unique'   => '用户名已存在',
            'mobile.regex'      => '手机号码格式不正确',
            'username.regex'    => '用户名必须是至少6位的字母、数字',
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.admin.edit', ['id' => $id]))->withErrors($validator)->withInput();
        }
        // 密码加密
        if ($input['password']) {
            $input['password'] = bcrypt($request->input('password'));
        }else{
            unset($input['password']);
        }
        $result = Admin::where('id', $id)->update($input);
        return $this->success(route('admin.admin.index'), '修改成功！');
    }

    /**
     * 删除
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $ids = $request->input('id');
        Admin::destroy($ids);
        return $this->success(route('admin.admin.index'), '删除成功！');
    }


}