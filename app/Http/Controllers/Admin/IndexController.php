<?php
/**
 * Created by PhpStorm.
 * User: nayo
 * Date: 2018/8/8
 * Time: 下午4:21
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;

class IndexController extends AdminController
{
    protected $redirectTo = '/admin/index';

    public function login(Request $request)
    {

        if ($request->isMethod('post')) {
            $request->flashOnly('username');

            /*表单数据校验*/
            $this->validate($request, [
                'username' => 'required',
                'password' => 'required|min:6',
                'captcha'  => 'required|captcha',
            ], [
                'captcha.required' => '请填写验证码',
                'captcha.captcha'  => '验证码错误',
            ]);


            /*只接收username和password的值*/
            $credentials = [
                'username' => $request->input('username'),
                'password' => $request->input('password'),
                'status'   => 1,
            ];

            /*根据邮箱地址和密码进行认证*/
            if (Auth::guard('admin')->attempt($credentials)) {

                /*认证成功后跳转到首页*/
                return redirect()->to(route('admin.index.index'));
            }

            /*登录失败后跳转到首页，并提示错误信息*/
            return redirect(route('admin.index.login'))
                ->withInput($request->only('username'))
                ->withErrors([
                    'password' => '用户名或密码错误，请核实！',
                ]);
        }

        return view("admin.index.login");

    }


    public function logout(Request $request)
    {

        Auth::guard('admin')->logout();
        $request->session()->flush();
        return redirect()->to(route('admin.index.login'));
    }


    public function index(Request $request)
    {
        return view('admin.index.index', [
                'systemInfo' => [],
            ]
        );
    }

    public function formSample()
    {
        return view('admin.index.form');
    }

    public function listSample()
    {
        return view('admin.index.list');
    }


    /*显示或隐藏sidebar*/
    public function sidebar(Request $request)
    {
        Cookie::forget('sidebar_collapse');
        $cookie = Cookie::forever('sidebar_collapse', $request->get('collapse'));
        return response()->json('ok')->withCookie($cookie);
    }

}