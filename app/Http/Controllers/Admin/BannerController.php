<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use App\Models\Experience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BannerController extends AdminController
{
    protected $rules = [
        'url'    => 'required',
        'status' => 'sometimes|integer',
        'sort'   => 'sometimes|integer',
        'logo'   => 'required|file',
    ];
    protected $message = [

    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->all();

        $query = Banner::query();

        /*状态过滤*/
        if (isset($filter['status']) && $filter['status'] > 0) {
            $query->where('status', '=', $filter['status']);
        }

        /*时间过滤*/
        if (isset($filter['date_range']) && $filter['date_range']) {
            $query->whereBetween('created_at', explode(" - ", $filter['date_range']));
        }

        $banners = $query->orderBy('created_at', 'desc')->paginate(20);
        return view("admin.banner.index")->with(compact('banners', 'filter'));
    }


    public function add(Request $request)
    {
        return view('admin.banner.add');
    }

    /**
     * 添加
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->message);

        if ($validator->fails()) {
            return redirect(route('admin.banner.add'))->withErrors($validator)->withInput();
        }
        $input = $request->only([ 'url', 'summary', 'status', 'sort']);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'banners/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['logo'] = str_replace('/', '-', $filePath);
        }
        $input = array_filter($input);
        $result = Banner::create($input);
        return $this->success(route('admin.banner.index'), '添加成功！');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $banner = Banner::find($id);
        if (empty($banner)) {
            abort(404);
        }
        return view('admin.banner.edit')->with(compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->rules['logo'] = 'sometimes|file';
        $validator = Validator::make($request->all(), $this->rules, $this->message);

        if ($validator->fails()) {
            return redirect(route('admin.banner.edit', ['id' => $id]))->withErrors($validator)->withInput();
        }
        $input = $request->only(['url', 'summary', 'status', 'sort']);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'banners/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['logo'] = str_replace('/', '-', $filePath);
        }
        $result = Banner::where('id', $id)->update($input);
        return $this->success(route('admin.banner.index'), '编辑成功！');
    }


    /**
     * 删除
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');
        Banner::destroy($id);
        return $this->success(route('admin.banner.index'), '删除成功');
    }
}
