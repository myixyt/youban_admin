<?php
/**
 * Created by PhpStorm.
 * User: nayo
 * Date: 2018/8/8
 * Time: 下午4:46
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Log;

class AdminController extends Controller
{
    public function __construct()
    {
        $notVerifiedData = [];
        View::share('sidebar_collapse', Cookie::get('sidebar_collapse', 0));
        View::share('notVerifiedData', $notVerifiedData);
    }

    protected function success($url,$message)
    {
        Session::flash('message',$message);
        Session::flash('message_type',2);
        return redirect($url);
    }


    protected function error($url,$message)
    {
        Session::flash('message',$message);
        Session::flash('message_type',1);
        return redirect($url);
    }
}