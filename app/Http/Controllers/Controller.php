<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Cache;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /**
     * 成功回调
     * @param $message
     */
    protected function ajaxSuccess($message){
        $data = array(
            'code' => 0,
            'message' => $message
        );
        return response()->json($data);
    }


    /**
     * 错误处理
     * @param $code
     * @param $message
     */
    protected function ajaxError($code,$message){
        $data = array(
            'code' => $code,
            'message' => $message
        );
        return response()->json($data);
    }

    /**
     * 业务层计数器
     * @param $key 计数器key
     * @param null $step 级数步子
     * @param int $expiration 有效期
     * @return Int count
     */
    protected function counter($key,$step=null,$expiration=1440){

        $count = Cache::get($key,0);
        /*直接获取值*/
        if( $step === null ){
            return $count;
        }

        $count = $count + $step;

        Cache::put($key,$count,$expiration);

        return $count;
    }
}
