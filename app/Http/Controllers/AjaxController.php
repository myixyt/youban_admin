<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\User;
use App\Services\SmsService;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AjaxController extends Controller
{

    public function sendSmsCode(Request $request)
    {

        if ($request->isMethod('post')) {
            $mobile = $request->input('mobile', '');
            if (!is_mobile($mobile)) {
                return $this->ajaxError(10004, '手机号格式码错误');
            }
            $sendType = $request->input('send_type', '');
            if ($sendType == 'register' && User::where("mobile", "=", $mobile)->count() > 0) { //注册发送处理
                return $this->ajaxError(10011, '该手机号已注册，不能重复注册');
            } else if ($sendType == 'doctor' && User::where("mobile", "=", $mobile)->where('role', 1)->count() == 0) { //找回密码处理
                return $this->ajaxError(10011, '该手机号不存在，请核实');
            } elseif ($sendType == 'see') { // 查看病例登录短信
                $user = User::where('mobile', $mobile)->first();
                if (empty($user)) {
                    return $this->ajaxError(10012, '对不起，用户不存在！');
                }
                $question = Question::where('user_id', $user->id)->first();
                if (empty($question)) {
                    return $this->ajaxError('10013', '对不起，您还未发起提问！');
                }
            }

            /*次数限制*/
            $sendTimes = $this->counter('send_sms_counter_' . $mobile);
            if ($sendTimes > config('tipask.sms_limit_times', 100)) {
                return $this->ajaxError(10005, '短信验证码发送数量已超出当日最大限制，请明天再试');
            }

            if (!SmsService::sendSmsCode($mobile)) {
                return $this->ajaxError(10006, '短信发送失败，请稍后再试');
            }
            $this->counter('send_sms_counter_' . $mobile, 1);
            return $this->ajaxSuccess("success");
        }
        return $this->ajaxError(10007, "请求错误");
    }

    public function getQrcode($id,$code)
    {

        if(!file_exists(public_path('qrcodes')))
            mkdir(public_path('qrcodes'));
        $url = 'https://h5.guangpyl.com/#/login?id='.$id.'&code='.$code;
//        $url = 'http://h5.guangpyl.com/#/?code='.$code.'&'.'id='.$id;

        $file = 'qrcodes/'.$code.'-nqrcode.png';
        if(file_exists(public_path($file))){
            return redirect()->to(url($file));
        }else{
            QrCode::format('png')->size(200)->generate($url, public_path($file));
            return redirect()->to(url($file));
        }
//        return url('qrcodes/'.$code.'-qrcode.png');
    }

}
