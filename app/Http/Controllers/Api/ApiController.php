<?php
/**
 * Created by PhpStorm.
 * User: sdf_sky
 * Date: 2018/10/17
 * Time: 下午5:08
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

class ApiController extends Controller
{
    protected $user;
    public function __construct(UserRepository $userRepository)
    {
        $token = Request::header('token');
        if ($token){
            $this->user = $userRepository->getUserInfoByToken($token);
        }
    }

    /**
     * 成功返回json数据
     * @param array $data
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonSuccess($data = [], $message = 'success')
    {
        $response = [
            'code'    => '0',
            'message' => $message,
            'body'    => $data,
        ];
        return response()->json($response);
    }

    /**
     * 发生错误是接口返回处理
     * @param $code string 错误码
     * @param $message string 错误内容
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonError($code, $message)
    {
        $response = [
            'code'    => $code,
            'message' => $message,
        ];
        return response()->json($response);
    }
}