<?php
/**
 * Created by PhpStorm.
 * User: nayo
 * Date: 2019/3/28
 * Time: 上午11:24
 */

namespace App\Http\Controllers\Api;

use App\Models\Activity;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Experience;
use App\Models\Letter;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class IndexController extends ApiController
{
    /**
     * 首页数据
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $code)
    {
        $doctor = User::where('code', $code)->first();
        if ( !empty($doctor)) {
            $doctor->increment('views'); // 扫码次数加1
        } else {
            $code = env('DEFAULT_CODE');
            $doctor = User::where('code', $code)->first();
            if (empty($doctor)){
                return $this->jsonError('4002','医生不存在！');
            }
//            $doctor->increment('views'); // 扫码次数加1
        }
        $response['doctor'] = $doctor;
        $response['banners'] = Banner::where(['status' => 1])->orderBy('sort', 'asc')->get();
        $response['articles'] = Article::where(['type' => 1, 'status' => 1])
            ->orderBy('created_at', 'desc')->limit(3)->get();
        $response['article_types'] = trans_article_type('all');
        return $this->jsonSuccess($response);
    }

    /**
     * 文章列表
     * @param $type
     * @param $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function articles($type, $page)
    {
        $pageSize = 100;
        $offset = ($page - 1) * $pageSize;
        $total = Article::where(['type' => $type, 'status' => 1])->count();
        $result['pages'] = ceil($total / $pageSize);
        $result['articles'] = Article::where(['type' => $type, 'status' => 1])->offset($offset)->limit($pageSize)
            ->orderBy('publish_date', 'desc')->orderBy('created_at', 'desc')->get();
        return $this->jsonSuccess($result);
    }

    // 文章详情
    public function detail($id)
    {
        $article = Article::where('id',$id)->first();
        if ($article){
            return $this->jsonSuccess($article);
        }
        return $this->jsonError('40004','未找到');
    }

    /**
     * 病友实录
     * @param $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function experiences($page)
    {
        $pageSize = 100;
        $offset = ($page - 1) * $pageSize;
        $total = Experience::where(['status' => 1])->count();
        $result['pages'] = ceil($total / $pageSize);
        $result['experiences'] = Experience::where(['status' => 1])->offset($offset)->limit($pageSize)->orderBy('sort', 'asc')->get();
        return $this->jsonSuccess($result);
    }

    /**
     * 肺腑之言
     * @param $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function letters($page)
    {
        $pageSize = 100;
        $offset = ($page - 1) * $pageSize;
        $total = Letter::where(['status' => 1])->count();
        $result['pages'] = ceil($total / $pageSize);
        $result['letters'] = Letter::where(['status' => 1])->offset($offset)->limit($pageSize)->orderBy('sort', 'asc')->get();
        return $this->jsonSuccess($result);
    }

    /**
     * 病友实录提交
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function postExperience(Request $request)
    {
        $rules = [
            'username' => 'required',
            'age'      => 'required|integer',
            'gender'   => 'required',
//            'district' => 'required',
//            'keywords' => 'required',
//            'disease'  => 'required',
            'content'  => 'required',
        ];
        $message = [
            'username.required' => '称呼不能为空',
            'age.required'      => '年龄不能为空',
            'district.required' => '地区不能为空',
            'disease.required'  => '病种不能为空',
            'content.required'  => '内容不能为空',
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return $this->jsonError(422, current($validator->errors()->messages())[0]);
        }
        $input = $request->only(['username', 'age', 'district', 'keywords', 'disease', 'content', 'status', 'sort', 'tel', 'gender']);
        $input['status'] = 4; // 审核后上线
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();
            $filePath = 'articles/' . gmdate("Y") . "/" . gmdate("m") . "/" . uniqid(str_random(8)) . '.' . $extension;
            Storage::disk('local')->put($filePath, File::get($file));
            $input['logo'] = str_replace('/', '-', $filePath);
        }
        $input = array_filter($input);
        $result = Experience::create($input);
        if ( !$result) {
            return $this->jsonError('999', '提交失败，请稍后重试！');
        }
        return $this->jsonSuccess();
    }

    /**
     * 提交信
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLetter(Request $request)
    {
        $rules = [
            'username' => 'required',
            'content'  => 'required',
        ];
        $message = [
            'username.required' => '称呼不能为空',
            'content.required'  => '内容不能为空',
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
            return $this->jsonError(422, current($validator->errors()->messages())[0]);
        }
        $input = $request->only(['username', 'content']);
        $input['status'] = 4; // 审核后上线
        $result = Letter::create($input);
        if ( !$result) {
            return $this->jsonError('999', '提交失败，请稍后重试！');
        }
        return $this->jsonSuccess();
    }
}