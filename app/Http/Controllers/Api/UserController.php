<?php
/**
 * Created by PhpStorm.
 * User: nayo
 * Date: 2019/3/28
 * Time: 上午11:24
 */

namespace App\Http\Controllers\Api;

use App\Models\Application;
use App\Models\Question;
use App\Models\User;
use App\Repositories\AppRepository;
use App\Repositories\SmsRepository;
use App\Repositories\UserRepository;
use App\Services\SmsService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends ApiController
{
    public function login(Request $request, UserRepository $userRepository)
    {
        $code = $request->input('code');
        $mobile = $request->input('mobile');
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|regex:/^1[34578]\d{9}$/',
            'code' => 'required|min:4|max:6',
            'bind_user' => 'required|integer',
        ], [
            'mobile.required' => '手机号不能为空',
            'mobile.regex' => '手机号码格式不正确',
            'code.required' => '验证码不能为空',
            'bind_user' => '专属医生不能为空',
        ]);
        if ($validator->fails()) {
            return $this->jsonError('422', current($validator->errors()->getMessages())[0]);
        }

        // 判断绑定用户是否存在
        $doctor = User::where('id', $request->input('bind_user'))->where('role', '=', 1)->first();
        if (empty($doctor)) {
            return $this->jsonError('4004', '专属医生不存在！');
        }

        // 校验登录短信
        if (SmsService::verifySmsCode($mobile, $code)) {
            $data = [
                'mobile' => $mobile,
                'bind_user' => $request->input('bind_user'),
                'admin_id' => $doctor->admin_id,
            ];
            $user = $userRepository->apiLogin($data);
            $res['user'] = $user;
            return $this->jsonSuccess($res);
        }
        return $this->jsonError('10007', '验证码输入错误，请核实');
    }

    // doctor短信验证码登录
    public function smsLogin(Request $request, UserRepository $userRepository)
    {
        $code = $request->input('code');
        $mobile = $request->input('mobile');
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|regex:/^1[34578]\d{9}$/',
            'code' => 'required|min:4|max:6',
        ], [
            'mobile.required' => '手机号码为必填项',
            'mobile.regex' => '手机号码格式不正确',
            'code.required' => '验证码不能为空',
        ]);

        if ($validator->fails()) {
            return $this->jsonError('422', current($validator->errors()->getMessages())[0]);
        }
        // 判断角色
        $exist = User::where('mobile',$mobile)->where('role',1)->first();
        if (empty($exist)){
            return $this->jsonError('40007','对不起，您无权操作！');
        }
        // 短信校验
        if (SmsService::verifySmsCode($mobile, $code)) {
            $user = $userRepository->login($mobile);
            $data['user'] = $user;
            $data['questions'] = $userRepository->questions($user->id);
            return $this->jsonSuccess($data);
        }
        return $this->jsonError('4001', '短信验证码错误！');
    }

    // 用户验证码登录
    public function userSmsLogin(Request $request, UserRepository $userRepository)
    {
        $code = $request->input('code');
        $mobile = $request->input('mobile');
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|regex:/^1[34578]\d{9}$/',
            'code' => 'required|min:4|max:6',
        ], [
            'mobile.required' => '手机号码为必填项',
            'mobile.regex' => '手机号码格式不正确',
            'code.required' => '验证码不能为空',
        ]);

        if ($validator->fails()) {
            return $this->jsonError('422', current($validator->errors()->getMessages())[0]);
        }
        // 判断角色
        $exist = User::where('mobile',$mobile)->where('role',2)->first();
        if (empty($exist)){
            return $this->jsonError('40007','对不起，您无权操作！');
        }
        // 短信校验
        if (SmsService::verifySmsCode($mobile, $code)) {
            $user = $userRepository->login($mobile);
            $data['user'] = $user;
            $data['questions'] = $userRepository->questions($user->id);
            return $this->jsonSuccess($data);
        }
        return $this->jsonError('4001', '短信验证码错误！');
    }


    // 病例列表
    public function questionsList(Request $request, UserRepository $userRepository)
    {
        $user_id = $this->user->id;
        if ($this->user->role != 1 || $this->user->status != 1) {
            return $this->jsonError('4002', '用户无效');
        }
        $page = $request->input('page', 1);
        $pageSize = $request->input('pageSize', 10);
        $data['questions'] = $userRepository->questions($user_id, $page, $pageSize);
        return $this->jsonSuccess($data);
    }

    public function ask(Request $request)
    {
        Log::info('ask_params', $request->all());
        $validateRules = [
            'to_user_id' => 'required|integer',
            'cycle' => 'required|in:1,2',
            'cycle_desc' => 'required',
            'expression' => 'required',
            'age' => 'required|integer',
            'symptom' => 'required',
            'description' => 'required',
        ];
        $message = [
            'to_user_id.required' => '被提问人不能为空',
            'cycle.required' => '例假周期不能为空',
            'expression.required' => '例假现象不能为空',
            'age.required' => '年龄不能为空',
            'symptom.required' => '症状不能为空',
            'description.required' => '问题描述不能为空',
        ];
        $data = [
            'user_id' => $this->user->id,
            'to_user_id' => $request->input('to_user_id'),
            'cycle_desc' => $request->input('cycle_desc'),
            'cycle' => $request->input('cycle'),
            'expression' => $request->input('expression'),
            'age' => $request->input('age'),
            'symptom' => $request->input('symptom'),
            'description' => $request->input('description'),
        ];

        $validator = Validator::make($data, $validateRules, $message);
        if ($validator->fails()) {
            return $this->jsonError('422', current($validator->errors()->getMessages())[0]);
        }
        $cycleDesc = Question::CYCLE_DESC;
        $expressionDesc = Question::EXPRESSION_DESC;

        $desc = $cycleDesc[$data['cycle']];

        // 判断字段类型
        if (!is_array($data['cycle_desc']) || !is_array($data['expression'])) {
            return $this->jsonError('422', '例假描述或例假现象数据不正确！');
        }
        // 判断经期描述是否合规
        foreach ($desc as $key => $val) {
            if (!array_key_exists($key, $data['cycle_desc'])) {
                return $this->jsonError('422', $key . '不存在！');
            }
        }

        // 判断景气描述是否正确
        $expressions = $data['expression'];
        foreach ($expressions as $key => $val) {
            if (!array_key_exists($key, $expressionDesc)) {
                return $this->jsonError('422', $key . '不存在！');
            }
        }

        $data['cycle_desc'] = json_encode($data['cycle_desc']);
        $data['expression'] = json_encode($data['expression']);

        // 创建问题
        $result = Question::create($data);
        return $this->jsonSuccess([], '提交成功！请耐心等待医生回复，医生回复后将以短信告知问诊结果！');
    }

    // 个人用户查看病例
    public function questions()
    {
        $user_id = $this->user->id;
        $questions = Question::where('user_id', $user_id)->get();
        if (empty($questions)) {
            return $this->jsonError('40003', '对不起，您还没发起提问！');
        }
        $questions->map(function ($question) {
            $question->cycle_desc = json_decode($question->cycle_desc);
            $question->expression = json_decode($question->expression);
            $question->create = dateStr(strtotime($question->created_at));
        });
        $data['data'] = $questions;
        return $this->jsonSuccess($data);
    }

    // 问题详情
    public function questionDetail(Request $request)
    {
        $user_id = $this->user->id;
        $id = $request->input('id');
        $validator = Validator::make($request->all(), ['id' => 'required|integer'], ['id.required' => 'id不能为空']);
        if ($validator->fails()) {
            return $this->jsonError('422', current($validator->errors()->getMessages())[0]);
        }
        $question = Question::where('id', $id)->where(function ($q) use ($user_id) {
            $q->where('user_id', $user_id)->orWhere('to_user_id', $user_id);
        })->first();
        if (!empty($question)) {
            $question->cycle_desc = json_decode($question->cycle_desc);
            $question->expression = json_decode($question->expression);
            $question->create_at = dateStr(strtotime($question->created_at));
            return $this->jsonSuccess($question);
        }
        return $this->jsonError('40005', '未找到您查看的病例！');
    }

    // 回复问题
    public function reply(Request $request)
    {
        $user_id = $this->user->id;
        $id = $request->input('id');
        $validator = Validator::make($request->all(), ['id' => 'required|integer',
            'reply' => 'required|max:512',
        ], ['id.required' => 'id不能为空', 'reply.required' => '回复内容不能为空']);
        if ($validator->fails()) {
            return $this->jsonError('422', current($validator->errors()->getMessages())[0]);
        }
        $question = Question::where('id', $id)->where('to_user_id', $user_id)->first();
        if (!empty($question)) {

            // 已经回复则不能重复
            if ($question->status == 1) {
                return $this->jsonError('40006', '对不起，您已经回复，请不要重复提交');
            }
            $question->reply = $request->input('reply');
            $question->reply_at = Carbon::now();
            $question->status = 1;
            $question->save();
            SmsService::sendInformCode($question->user->mobile);
            return $this->jsonSuccess([], '您已回复成功！');
        }
        return $this->jsonError('40005', '未找到您查看的病例！');
    }

    // 发起积分申请
    public function apply(Request $request)
    {
        $user_id = $this->user->id;
        if ($this->user->role != 1) {
            return $this->jsonError('40006', '对不起，您无权发起申请！');
        }
        $data = [
            'user_id' => $user_id,
            'mobile' => $this->user->mobile,
            'status' => 0,
        ];
        Application::firstOrCreate($data);
        return $this->jsonSuccess([], '已发送，请等待工作人员与您联系！');
    }


    // doctor首页
    public function index(Request $request, UserRepository $userRepository)
    {
        $data['user'] = $this->user;
        $data['questions'] = $userRepository->questions($this->user->id);
        return $this->jsonSuccess($data);
    }
}