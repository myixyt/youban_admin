<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/1
 * Time: 14:15
 */

namespace App\Http\Middleware\Api;


use App\Services\Crypt\Api\Aes;
use App\Services\Crypt\Api\PHPJsAes;
use Closure;
use Illuminate\Support\Facades\Config;

class Encryption
{
    public $except = [
    ];

    public function handle($request, Closure $next )
    {
        if ($this->shouldPassThrough($request)) {
            return $next($request);
        }

        $clientKey = $request->input('client_key');
        $response = $next($request);

        $returnData = json_decode($response->getContent(),true);
        $returnData['encrypt'] = false; // 传入参数是否要解密

        if (needEncrypt()) {
            $encryptData = '';
            if (Config::get('api.client_key.h5') != $clientKey && isset($returnData['data'])) {
                $encryptData = Aes::newEnAes(json_encode($returnData['data']));
            } else if(isset($returnData['data'])) {
                $encryptData = PHPJsAes::encrypt(json_encode($returnData['data']));
            }

            $returnData['data'] = $encryptData; // 加密数据
            $returnData['encrypt'] = true; // 传入参数是否要解密
        }


        $response->setContent(json_encode($returnData));
        return $response;
    }

    public function shouldPassThrough($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->is($except)) {
                return true;
            }
        }

        return false;
    }

}