<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/1
 * Time: 10:34
 */

namespace App\Http\Middleware\Api;


use App\Services\ApiStatusCode;
use App\Services\CacheKey;
use App\Services\Crypt\Api\Aes;
use App\Services\Crypt\Api\PHPJsAes;
use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class Authorize
{
    private $paramsShouldIgnore = [
        's',
    ];

    public function handle(Request $request, Closure $next )
    {
        Log::info($request->all());
        //接收全部参数。排除掉 key为s的参数
        $params = $request->except($this->paramsShouldIgnore);

        //检测参数是否有遗漏
        $paramsCheckRes = $this->paramsCheck($params);
        if (0 !== $paramsCheckRes['code']) {
             return response()->json($paramsCheckRes);
        }

        //检测参数是否为合法客户端
        $clientCheckRes = $this->clientCheck($params);
        if (0 !== $clientCheckRes['code']) {
             return response()->json($clientCheckRes);
        }

        //检测传递数据格式是否异常
        $paramsFormCheckRes = $this->paramsFormatCheck($params, $request);
        if (0 !== $paramsFormCheckRes['code']) {
             return response()->json($paramsFormCheckRes);
        }

        //检测重放攻击
        $replayCheckRes = $this->replayCheck($params['timestrap'], $params['nonce']);
        if (0 !== $replayCheckRes['code']) {
             return response()->json($replayCheckRes);
        }

        //客户端身份校验
        $clientAuthCheckRes = $this->clientAuthCheck($request, $params);
        if (0 !== $clientAuthCheckRes['code']) {
             return response()->json($clientAuthCheckRes);
        }

        return $next($request);
    }

    /**
     * 客戶端身份校验(API_KEY方式校验)
     * @param $request
     * @param array $params
     * @return \Illuminate\Http\JsonResponse
     */
    private function clientAuthCheck($request, array $params)
    {
        //分解参数
        extract($params);
        //获取路由
        $restApi = $request->path();

        unset($params['sign']);

        ksort($params);
        $ret = '';
        foreach ($params as $key => $val) {
            $ret .= "{$key}={$val}&";
        }

        $str = rtrim($ret, '&');
        // 计算签名，和客户端传过来的做比较
        $encodedStr = base64_encode($str . $restApi . Config::get('api.security_key'));
        $serverSign = md5($encodedStr);

        if ($serverSign != $sign) {
            return res([], ApiStatusCode::INVALID_SIGN);
        }

        //$nonce数据缓存,默认600秒失效
        $key = CacheKey::getNonceCacheKey($nonce);

        Redis::pipeline(function ($pipe) use ($key, $nonce) {
            $pipe->set($key, $nonce);
            $pipe->expire($key, Config::get('api.replay_limit_time'));
        });
        return res([], ApiStatusCode::OK);
    }

    /**
     * 检测重放
     * @param $timestrap
     * @param $nonce
     * @return \Illuminate\Http\JsonResponse
     */
    private function replayCheck($timestrap, $nonce)
    {
        //开始检测重放攻击
        //请求时间和当前时间差距超过重放限制时间,拒绝请求
        if (($timestrap + Config::get('api.replay_limit_time')) < time()) {
            return res([], ApiStatusCode::REPLAY_TIME_EXCEEDED);
        }
        //请求时间和当前时间差距未超过重放限制时间,则需要检查请求的唯一性,拒绝请求
        $key = CacheKey::getNonceCacheKey($nonce);
        if (Redis::get($key) == $nonce) {
            return res([], ApiStatusCode::DUPLICATED_REQUEST);
        }
        return res([], ApiStatusCode::OK);
    }


    /**
     * 检测传递数据格式是否异常
     * @param array $params
     * @param $request
     * @return mixed
     * @throws \Exception
     */
    private function paramsFormatCheck(array $params, &$request)
    {
        if (needEncrypt()) {
            //开始解密参数
            if (Config::get('api.client_key.h5') != $params['client_key']) {
                try {
                    $data = Aes::newDeAes($params['data']);
                } catch (\Exception $e) {
                    return res([], ApiStatusCode::DECRYPTION_FAILED);
                }
            } else {
                //$params['data'] = 'iTaA/CnSI1OohJSlG8IZ9W1Ejeqn51FgsffE+P2tOUTUOJw50g7Ber6Yn8tm2e3E';
                $data = PHPJsAes::decrypt($params['data']);
                Log::info(
                    [
                        'encrypt' => $params['data'],
                        'decrypt' => $data,
                    ]
                );
            }

            if (!$data) {
                return res([], ApiStatusCode::DECRYPTION_FAILED);
            }
        } else {
            $data = $params['data'];
        }

        //如参数解密成功,则进行替换
        unset($request['data']);

        $ret = json_decode($data, true);

        if (!$ret) {
            $ret = [];
        }
        Log::info([
            'param' => $ret,
            'url' => $request->url(),
        ]);
        $request->merge($ret);
        return res([], ApiStatusCode::OK);
    }


    private function clientCheck($params)
    {
        if (!in_array($params['client_key'], array_values(Config::get('api.client_key')))) {
            return res([], ApiStatusCode::INVALID_CLIENT);
        }
        return res([], ApiStatusCode::OK);
    }

    /**
     * 检查参数是否有遗漏
     * @param array $params
     * @return mixed
     */
    private function paramsCheck(array $params)
    {
        //必备参数
        $needParams = Config::get('api.need_params');

        //检查是否丢失参数
        $missParams = array_diff($needParams, array_keys($params));

        if (!empty($missParams)) {
            return res([], ApiStatusCode::PARAMS_LACK, ['params' => implode(',', $missParams)]);
        }
        return res([], ApiStatusCode::OK);
    }

}