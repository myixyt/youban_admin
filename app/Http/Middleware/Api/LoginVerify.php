<?php
/**
 * Created by PhpStorm.
 * User: renyimin
 * Date: 2017/4/27
 * Time: 19:23
 */

namespace App\Http\Middleware\Api;

use App\Repositories\UsersRepository;
use App\Services\ApiStatusCode;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginVerify
{
    public function handle(Request $request, Closure $next)
    {
        $repository = new UsersRepository();
        //通过token获取的用户信息
        if ( !$request->hasHeader('token')) {
            return response()->json(
                [
                    'code'    => 422,
                    'message' => 'token不存在',
                ]
            );
        }
        $token = $request->header('token', null);
        if ( !$token) {
            return response()->json([
                'code'    => ApiStatusCode::INVALID_TOKEN,
                'message' => ApiStatusCode::$login[ApiStatusCode::INVALID_TOKEN],
            ]);
        }

        $user = $repository->getUserInfoByToken($token);
        if (empty($user)) {
            return response()->json([
                'code'    => ApiStatusCode::INVALID_TOKEN,
                'message' => ApiStatusCode::$login[ApiStatusCode::INVALID_TOKEN],
            ]);
        }
        if ($user->id != $repository->decodeToken($request->header('token'))) {
            return response()->json([
                'code'    => ApiStatusCode::ILLEGAL_TOKEN,
                'message' => ApiStatusCode::$login[ApiStatusCode::ILLEGAL_TOKEN],
            ]);
        }
        return $next($request);
    }
}