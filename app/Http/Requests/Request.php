<?php
/**
 * User: ${}
 * Date: 2018/8/28
 * Time: 下午12:30
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

class Request extends FormRequest
{
    /**
     * 验证规则
     * @return array
     */
    public function rules()
    {
        return $this->getRulesByActionName();
    }

    /**
     * 根据当前请求的方法名
     * @return array
     */
    protected function getRulesByActionName()
    {
        // 获取当前执行的控制器名和动作名
        list(, $action) = explode('@', $this->route()->getActionName());
        // 返回验证规则的方法名
        $rulesMethodName = 'get' . ucfirst($action) . 'Rules';
        if (method_exists($this, $rulesMethodName)) {
            $rules = $this->$rulesMethodName();
            if (is_array($rules)) {
                return $rules;
            }
        }
        return [];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator,$this->response(
            $this->formatErrors($validator)));
    }

    protected function formatErrors(Validator $validator)
    {
        $message = $validator->errors()->all();
        $result = [
            'msg' => $message,
            'code' => 422,
        ];
        return $result;
    }

}