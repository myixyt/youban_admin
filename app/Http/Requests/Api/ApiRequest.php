<?php
/**
 * User: ${}
 * Date: 2018/8/28
 * Time: 下午12:36
 */

namespace App\Http\Requests\Api;


use App\Http\Requests\Request;

abstract class ApiRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function response()
    {
        return response()->json(res([], 422, null, current($errors)[0]));
    }

}