<?php

namespace App\Http\Requests\Api;

class LoginRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getTestRules()
    {
        return [
            'name' => 'required',
            'code' => 'required'
        ];
    }

}
