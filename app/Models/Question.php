<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const EXPRESSION_DESC = [ // 例假现象
        'color'     => [1 => '浅', 2 => '适中', 3 => '适中'], // 颜色
        'blood'     => [1 => '少', 2 => '无', 3 => '多'], // 血块
        'volume'    => [1 => '少', 2 => '适中', 3 => '多'], // 经量
        'viscosity' => [1 => '稀', 2 => '适中', 3 => '稠'], //粘稠度
    ];

    const CYCLE_DESC = [
        '1' => [
            'interval_time' => '间隔时间',
            'duration'      => '持续时间',
        ],
        '2' => [
            'description' => '不定期描述',
        ],
    ];

    protected $table = 'questions';
    public $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User')->withDefault();
    }

    public function hasOneUser()
    {
        return $this->hasOne(User::class, 'id', 'to_user_id')->withDefault();
    }

}
