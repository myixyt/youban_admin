<?php

namespace App\Models;

use App\Models\Relations\ShowLogoTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Activity
 *
 * @property int $id
 * @property string $topic 话题
 * @property string|null $preferred_date 举办日期
 * @property string $preferred_time 举办时间
 * @property string $address 举办地址
 * @property string $logo 图片
 * @property string $url 详情链接
 * @property string $experts 专家
 * @property string $live_url 直播地址
 * @property string $apply_url 报名地址
 * @property int $is_current 是否本期
 * @property int $sort 排序
 * @property int $status 状态
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $logo_url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereApplyUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereExperts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereIsCurrent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereLiveUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity wherePreferredDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity wherePreferredTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereTopic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereUrl($value)
 * @mixin \Eloquent
 */
class Activity extends Model
{
    use ShowLogoTrait;
    protected $table = 'activities';
    protected $guarded = [];
    public $appends = ['logo_url'];

}
