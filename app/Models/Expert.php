<?php

namespace App\Models;

use App\Models\Relations\ShowLogoTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * App\Models\Expert
 *
 * @property int $id
 * @property string $username 姓名
 * @property string $logo 头像
 * @property string $title 头衔
 * @property string $summary 简介
 * @property int $sort 排序
 * @property int $status 状态
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $logo_url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Expert whereUsername($value)
 * @mixin \Eloquent
 */
class Expert extends Model
{
    use ShowLogoTrait;
    protected $table = 'experts';
    protected $guarded = [];
    public $appends = ['logo_url'];

    public function loadExperts()
    {
        return $this->orderBy('sort','ASC')->pluck('username','id');
    }

}
