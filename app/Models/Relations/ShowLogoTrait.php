<?php
/**
 * Created by PhpStorm.
 * User: nayo
 * Date: 2019/3/28
 * Time: 下午1:59
 */
namespace App\Models\Relations;

trait ShowLogoTrait
{
    public function getLogoUrlAttribute()
    {
        if ($this->original['logo']) {
            return route('website.image.show', ['image' => $this->original['logo']]);
        }
        return '';
    }
}