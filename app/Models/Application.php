<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{

    protected $table = 'applications';
    public $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
