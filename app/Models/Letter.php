<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Letter
 *
 * @property int $id
 * @property string $username 昵称
 * @property string $content 告白内容
 * @property int $sort 排序
 * @property int $status 状态:4-下线，1-上线
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class Letter extends Model
{
    protected $table = 'letters';
    protected $guarded = [];
}
