<?php

namespace App\Models;

use App\Models\Relations\ShowLogoTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * App\Models\Experience
 *
 * @property int $id
 * @property string $username 昵称
 * @property int $age 年龄
 * @property string $district 地区
 * @property string $disease 病种
 * @property string $logo logo
 * @property string $keywords 关键词
 * @property string $content 内容
 * @property int $sort 排序
 * @property int $status 状态0-下线,1-上线
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $logo_url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereDisease($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Experience whereUsername($value)
 * @mixin \Eloquent
 */
class Experience extends Model
{
    use ShowLogoTrait;
    protected $table = 'experiences';
    protected $guarded = [];
    public $appends = ['logo_url'];


}
