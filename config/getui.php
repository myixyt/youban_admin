<?php
return [
    "APPKEY" => env('PUSHER_APP_KEY',''),
    "APPID" => env('PUSHER_APP_ID',''),
    "MASTERSECRET" => env('PUSHER_APP_MASTER_SECRET',''),
    "HOST" => [
        "http://sdk.open.api.igexin.com/apiex.htm",
        "https://sdk.open.api.igexin.com/apiex.htm"
    ],
    "CID" => "",
    // 此处true为https域名，false为http，默认为false。
    "HTTPS" => false,
];