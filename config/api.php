<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/1
 * Time: 10:45
 */

return [
    /*
     * 应用Api加密，签名验证相关配置。
     */

    // 是否启用https
    'https_on' => env('HTTPS_ON',false),
    'sql_log_on' => env('SQL_LOG_ON',false),

    'security_key' => env('API_SECURITY_KEY', 'j@(;Va.*N1k3zB9M?@K2'),

    'client_key' => [
        'h5' => env('H5', '5hW5uI9yki5kn6ztAo3y3uzB4CDkImK2'),
        'android' => env('ANDROID', 'CY9jxS7YDykTUU65DNuLQgJx2dP1bPEo'),
        'ios' => env('IOS', 'AXj9oEg0vk6KElkOSZ5KvsAOhmJm56EE'),
    ],

    'replay_limit_time' => 1200,
    'api_debug' => env('API_DEBUG', false),
    'aes_key_app' => env('', 'a_pXutkc616fyfxftvvuZa;ps'),
    //'aes_key_app' => env('', 'a_pXutkc616fyfxf'),
    'iv_app' => env('', '3C427D362E46467E6372735F7B54552E'),

    'aes_key_h5' => env('', '_p&+fxftvvuZa;ps'),
    'iv_h5' => env('', '8923123467281962'),

    'need_params' => [
        'client_key',
        'token',    // 主要用于登录验证
        'timestrap',
        'nonce',
        'sign', // 参数签名
        'data', // 加密后的参数数据
        'device_id', // 设备id
    ],

    'unavailable_params' => [
        'clientKey',
        'token',
        'timestrap',
        'nonce',
        'sign',
    ],

    'api_response_encryption' => env('API_RESPONSE_ENCRYPTION', true),

    //'ios_download' => env('IOS_DOWNLOAD', ''),       // ios下载地址
    //'android_apk_name' => env('ANDROID_APK_NAME', ''), // 安卓apk的包名称
    'token_prefix' => 'shuke_prefix_',
];