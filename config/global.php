<?php
/**
 * User: ${yang}
 * Date: 2018/8/29
 * Time: 下午2:19
 */
return [
    'version'       => '书刻 V1.0',
    //功能介绍
    'features'      => '',
    'versionConfig' => [
        'new_version'  => 0,  //最新版本号
        'force_update' => false,
        'content'      => '',
    ],

    // app Tab
    'tabs'          => [
        '1' => '推荐',
        '2' => '男生',
        '3' => '女生',
        '4' => '新书',
        '5' => '完结',
    ],

    // 每个tab，每个列表推荐6本书籍
    'tab_list'      => [
        '1' => [
            'readers' => [
                'title' => '最热小说',
                'list'  => [1, 2, 3, 4, 5, 6],
            ],
            'latest'  => [
                'title' => '最新小说',
                'list'  => [7, 8, 9, 10, 11, 12],
            ],
        ],
        '2' => [
            'readers' => [
                'title' => '最热小说',
                'list'  => [1, 2, 3, 4, 5, 6],
            ],
            'latest'  => [
                'title' => '最新小说',
                'list'  => [7, 8, 9, 10, 11, 12],
            ],
        ],
        '3' => [
            'readers' => [
                'title' => '最热小说',
                'list'  => [1, 2, 3, 4, 5, 6],
            ],
            'latest'  => [
                'title' => '最新小说',
                'list'  => [7, 8, 9, 10, 11, 12],
            ],
        ],
        '4' => [
            'readers' => [
                'title' => '最热小说',
                'list'  => [1, 2, 3, 4, 5, 6],
            ],
            'latest'  => [
                'title' => '最新小说',
                'list'  => [7, 8, 9, 10, 11, 12],
            ],
        ],
        '5' => [
            'readers' => [
                'title' => '最热小说',
                'list'  => [1, 2, 3, 4, 5, 6],
            ],
            'latest'  => [
                'title' => '最新小说',
                'list'  => [7, 8, 9, 10, 11, 12],
            ],
        ],

    ],
];