<?php

/*
 * This file is part of ibrand/laravel-sms.
 *
 * (c) iBrand <https://www.ibrand.cc>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'route' => [
        'prefix' => 'sms',
        'middleware' => ['web'],
    ],

    'easy_sms' => [
        'timeout' => 5.0,

        // 默认发送配置
        'default' => [
            // 网关调用策略，默认：顺序调用
            'strategy' => \Overtrue\EasySms\Strategies\OrderStrategy::class,

            // 默认可用的发送网关
            'gateways' => [
                'errorlog',
            ],
        ],

        // 可用的网关配置
        'gateways' => [
            'errorlog' => [
                'file' => storage_path('logs/laravel-sms.log'),
            ],

            'yunpian' => [
                'api_key' => '824f0ff2f71cab52936axxxxxxxxxx',
            ],

            'aliyun' => [
                'access_key_id' => env('ALI_SMS_ACCESS_KEY',''),
                'access_key_secret' => env('ALI_SMS_SECRET',''),
                'sign_name' => env('ALI_SIGN_NAME',''),
                'code_template_id' => env('ALI_TEMPLATE_ID',''),
            ],

            'alidayu' => [
                //...
            ],
        ],
    ],

    'code' => [
        'length' => 5,
        'validMinutes' => 5,
        'maxAttempts' => 0,
    ],

    'data' => [
        'product' => '',
    ],

    'dblog' => false,

    'content' => '【your app signature】亲爱的用户，您的验证码是%s。有效期为%s分钟，请尽快验证。',

    'storage' => \iBrand\Sms\Storage\CacheStorage::class,
];
