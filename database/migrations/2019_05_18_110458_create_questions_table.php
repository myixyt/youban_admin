<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default('0')->comment('用户ID');
            $table->unsignedInteger('to_user_id')->default('0')->comment('医生ID');
            $table->unsignedTinyInteger('cycle')->default('1')->comment('例假周期:1-定期,2-不定期');
            $table->string('cycle_desc')->default('')->comment('周期');
            $table->string('expression',512)->default('')->comment('表现');
            $table->unsignedTinyInteger('age')->default('0')->comment('年龄');
            $table->string('symptom','512')->default('')->comment('症状');
            $table->string('description','512')->default('')->comment('描述');
            $table->string('reply','512')->default('')->comment('回复内容');
            $table->unsignedTinyInteger('status')->default('0')->comment('1-已回复，2-未回复');
            $table->timestamp('reply_at')->nullable()->comment('回复时间');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
