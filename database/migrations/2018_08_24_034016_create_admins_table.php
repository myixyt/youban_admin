<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->char('mobile', 11)->default(0);
			$table->string('username')->default('');
			$table->char('password', 64)->default('');
			$table->boolean('status')->default(1)->comment('账号状态  0、禁用   1、启用');
			$table->timestamps();
		});

        DB::table('admins')->insert([
            ['id' => 1, 'mobile' => '13888888888', 'username' => 'admin', 'password' => bcrypt('123456'), 'status' => 1],
        ]);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admins');
	}

}
