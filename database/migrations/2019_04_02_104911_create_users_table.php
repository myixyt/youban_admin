<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mobile')->default(0)->comment('手机号码');
            $table->string('email',64)->default('')->comment('邮箱');
            $table->string('password',64)->default('')->comment('密码');
            $table->string('nickname','32')->default('')->comment('姓名');
            $table->string('wechat')->default('')->comment('简介');
            $table->tinyInteger('role')->default('1')->comment('1-医生,2-用户');
            $table->string('title','32')->default('')->comment('职称');
            $table->integer('age')->default('0')->comment('年龄');
            $table->string('avatar',128)->default('')->comment('头像');
            $table->string('youzan_url','512')->default('')->comment('有赞链接');
            $table->text('content')->nullable()->comment('介绍');
            $table->unsignedInteger('bind_user')->default('0')->comment('扫码医生ID');
            $table->unsignedInteger('views')->default('0')->comment('扫码人数');
            $table->unsignedInteger('status')->default('1')->comment('1-可用，0禁用');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
