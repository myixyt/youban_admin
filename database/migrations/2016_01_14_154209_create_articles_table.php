<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned()->default(0);
            $table->string('title', 255)->default('')->comment('文章标题');
            $table->tinyInteger('type')->default(0)->comment('文章类型');
            $table->string('logo', 255)->default('')->comment('文章logo');
            $table->string('source', 128)->default('')->comment('文章来源');
            $table->string('keywords', 128)->default('')->comment('文章关键词');
            $table->string('summary', 255)->default('')->comment('导读、摘要');
            $table->text('content')->comment('文章内容');
            $table->date('publish_date')->nullable()->comment('发布日期');
            $table->integer('views')->unsigned()->default(0)->comment('查看数');
            $table->tinyInteger('status')->default(1)->comment('状态0-下线,1-上线');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
