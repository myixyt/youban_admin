$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
function show_form_error(element,msg){
    element.parent().addClass('has-error');
    if(element.parent().find(".help-block").size() > 0){
        element.parent().find(".help-block").html(msg);
    }else{
        element.after('<span class="help-block">'+msg+'</span>');
    }
}